//
//  GlobalMethods.m
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 11/13/15.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import "GlobalMethods.h"

@implementation GlobalMethods

+ (NSManagedObjectContext *)getContext
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    return [appDelegate managedObjectContext];
}

+ (NSArray *)fetchAllWithEntityName:(NSString *)entityName
{
    NSManagedObjectContext *context = [self getContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    return fetchedObjects;
}

+ (User *)getUser
{
    User *user;
    
    NSArray *users = [self fetchAllWithEntityName:@"User"];
    
    if ([users count] > 0)
        user = [users objectAtIndex:0];
    
    return user;
}

+ (void)saveUserWithInfo:(NSDictionary *)userInfo
{
    NSManagedObjectContext *context = [self getContext];
    User *user = [self getUser];
    
    if (user == nil)
        user = [NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:context];
    
    user.email = userInfo[@"email"];
    user.firstName = userInfo[@"firstName"];
    user.userId = userInfo[@"id"];
    user.lastName = userInfo[@"lastName"];
    user.middleName = userInfo[@"middleName"];
    user.userStatus = userInfo[@"userStatus"];
    user.userName = userInfo[@"username"];
    user.birthday = userInfo[@"birthday"];
    user.mobileNo = userInfo[@"mobileNo"];
    
    NSLog(@"USER %@", user);
    
    NSError *error;
    if (![[self getContext] save:&error])
        NSLog(@"failed to save");
}

+ (void)saveUserWithImage:(UIImage *)image
{
    NSManagedObjectContext *context = [self getContext];
    User *user = [self getUser];
    
    if (user == nil)
        user = [NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:context];
    
    user.displayImage =  UIImagePNGRepresentation(image);
    
    NSError *error;
    if (![[self getContext] save:&error])
        NSLog(@"failed to save");
}

+ (BOOL)isConnected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    
    if (networkStatus == NotReachable)
        NSLog(@"NO INTERNET CONNECTION");
    
    return networkStatus != NotReachable;
}

+ (void)saveUserStatus:(BOOL)status
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setObject:[NSNumber numberWithBool:status] forKey:@"userStatus"];
    
    [defaults synchronize];
}

+ (BOOL)getUserStatus
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults objectForKey:@"userStatus"];
}

+ (void)saveParkingList:(NSArray *)list
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:list forKey:@"parkingList"];
    [defaults synchronize];
}

+ (NSArray *)getCurrentParkingList
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults objectForKey:@"parkingList"];
}

+ (void)saveParkingListIndex:(NSInteger)index
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setObject:[NSNumber numberWithInteger:index] forKey:@"parkingListIndex"];
    
    [defaults synchronize];
    
    NSLog(@"Parking List index saved.");
}

+ (NSInteger)getCurrentParkingListIndex
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    return [[defaults objectForKey:@"parkingListIndex"] integerValue];
}

+ (void)logout
{    
    //[[[SDWebImageManager sharedManager] imageCache] clearDisk];
    //[[[SDWebImageManager sharedManager] imageCache] clearMemory];
    
    NSManagedObjectContext *context = [self getContext];
    User *user = [self getUser];
    [context deleteObject: user];
    
    NSError *error;
    if (![context save:&error]) {
        NSLog(@"failed to save");
    }
    
    NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate setRootViewControllerWithSB:@"LoginSB"];
}

+ (UIAlertController *)showAlertViewWithTitle:(NSString *)title message:(NSString *)message
{
    UIAlertController *alert =   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction
                         actionWithTitle:@"Ok"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    
    [alert addAction:ok];
    
    return alert;

}

+ (BOOL)NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

+ (BOOL)checkInputForWhiteSpaces:(NSString *)inputString
{
    NSCharacterSet *charSet = [NSCharacterSet whitespaceCharacterSet];
    NSString *trimmedString = [inputString stringByTrimmingCharactersInSet:charSet];
    
    if ([trimmedString isEqualToString:@""] || (trimmedString == (id)[NSNull null]) || ([trimmedString length] == 0))
    {
        return NO;
    }
    
    return YES;
}



@end
