//
//  LocationManager.m
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 11/25/15.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import "LocationManager.h"

@implementation LocationManager

+ (LocationManager *)sharedInstance
{
    static dispatch_once_t once;
    static LocationManager *manager;
    
    dispatch_once(&once, ^{
        manager = [[LocationManager alloc] init];
    });
    
    return manager;
}

- (void)initLocationManager
{
    if (!self.locationManager)
        self.locationManager = [[CLLocationManager alloc] init];
    
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8) {
        [self.locationManager requestAlwaysAuthorization];
    }
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 9) {
        if([self.locationManager respondsToSelector:@selector(allowsBackgroundLocationUpdates)]){
            [self.locationManager setAllowsBackgroundLocationUpdates:YES];
        }
    }
    
    [self.locationManager startUpdatingLocation];
    [self.locationManager startMonitoringSignificantLocationChanges];
}

#pragma mark - Delegate
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
  //  UIAlertView *errorAlert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"There was an error retrieving your location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
   // [errorAlert show];
    NSLog(@"Error: %@",error.description);
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *loc = [locations lastObject];
    
    NSLog(@"LOC: %f", loc.coordinate.latitude);
    NSLog(@"LOC: %f", loc.coordinate.longitude);
    
    [self.delegate getCurrentUserLocation:loc];
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    NSLog(@"did change status");
    
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined) {
        NSLog(@"not determined");
        
        [self requestAlwaysAuthorization];
        
    } else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways) {
        NSLog(@"Authorized");
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Location Services Authorized" object:self];
        
    } else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusRestricted){
        NSLog(@"restricted");
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Location Services Restricted" object:self];
        [self requestAlwaysAuthorization];
        
    } else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied) {
        NSLog(@"denied");
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Location Services Denied" object:self];
        
        [self requestAlwaysAuthorization];
    } else {
        NSLog(@"can not");
    }
}

- (void)requestAlwaysAuthorization
{
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];

    if (status == kCLAuthorizationStatusAuthorizedWhenInUse || status == kCLAuthorizationStatusDenied)
    {
        NSString *title;
        
        title = (status == kCLAuthorizationStatusDenied) ? @"Location services are off" : @"Background location is not enabled";
        
        NSString *message = @"To use background location you must turn on 'Always' in the Location Services Settings";
        
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:title
                                                                       message:message
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *settings = [UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction *action) {
                                                         NSURL *settingsURL = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                                         [[UIApplication sharedApplication] openURL:settingsURL];
                                                         
                                                     }];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction *action) {
                                                           [alert dismissViewControllerAnimated:YES completion:nil];
                                                       }];
        
        [alert addAction:cancel];
        [alert addAction:settings];
        
        [self presentViewController:alert animated:YES completion:nil];
    }

    else if (status == kCLAuthorizationStatusNotDetermined)
        [self.locationManager requestAlwaysAuthorization];
}



@end
