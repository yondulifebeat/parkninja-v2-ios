//
//  TransactionModel.m
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 12/29/15.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import "TransactionModel.h"

@implementation TransactionModel

- (id)initWithTransactionDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    
    if (self)
    {
        //self.parkingId = [[dictionary objectForKey:@"carId"] integerValue];
        //self.make = [dictionary objectForKey:@"make"];
        //self.model = [dictionary objectForKey:@"model"];
        //self.color = [dictionary objectForKey:@"color"];
        //self.plateNum = [dictionary objectForKey:@"plateNo"];
        
        self.email = [dictionary objectForKey:@"email"];
        self.reserveFrom = [[dictionary objectForKey:@"reserveFrom"] longValue];
        self.reserveTo = [[dictionary objectForKey:@"reserveTo"] longValue];
        //self.earlyBird = [dictionary objectForKey:@"earlyBird"];
        //self.parkingId = [[dictionary objectForKey:@"parkingLotId"] integerValue];
        
        //self.promoId = [[dictionary objectForKey:@"promoCodeId"] integerValue];
        
        self.estimateAmount = [[dictionary objectForKey:@"estimateAmount"] integerValue];
        
        self.transactionCode = [dictionary objectForKey:@"transactionCode"];
       //self.merchantPartnerId = [[dictionary objectForKey:@"merchantPartnerId"] integerValue];
        self.parkingLocation = [dictionary objectForKey:@"parkingLocation"];
        self.merchantName = [dictionary objectForKey:@"merchantName"];
        self.isActive = [[dictionary objectForKey:@"isActive"] boolValue];
        
    }
    
    return self;
}
@end
