//
//  CarModel.h
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 12/15/15.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CarModel : NSObject

@property (assign, nonatomic) NSInteger carId;
@property (strong, nonatomic) NSString *make;
@property (strong, nonatomic) NSString *model;
@property (strong, nonatomic) NSString *color;
@property (strong, nonatomic) NSString *plateNum;
@property (strong, nonatomic) NSString *status;
@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSString *user;

- (id)initWithCarDictionary:(NSDictionary *)dictionary;

@end
