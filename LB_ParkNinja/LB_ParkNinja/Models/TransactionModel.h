//
//  TransactionModel.h
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 12/29/15.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TransactionModel : NSObject

@property (assign, nonatomic) NSInteger carId;
@property (strong, nonatomic) NSString *make;
@property (strong, nonatomic) NSString *model;
@property (strong, nonatomic) NSString *color;
@property (strong, nonatomic) NSString *plateNum;
@property (strong, nonatomic) NSString *email;
@property (assign, nonatomic) long reserveFrom;
@property (assign, nonatomic) long reserveTo;
@property (strong, nonatomic) NSString *earlyBird;
@property (assign, nonatomic) NSInteger parkingId;
@property (assign, nonatomic) NSInteger promoId;
@property (assign, nonatomic) NSInteger estimateAmount;
@property (strong, nonatomic) NSString *transactionCode;
@property (assign, nonatomic) NSInteger merchantPartnerId;
@property (strong, nonatomic) NSString *parkingLocation;
@property (strong, nonatomic) NSString *merchantName;
@property (assign, nonatomic) BOOL isActive;

- (id)initWithTransactionDictionary:(NSDictionary *)dictionary;

@end
