//
//  ParkingLotModel.h
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 11/25/15.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TransactionModel.h"

@interface ParkingLotModel : NSObject

@property (assign, nonatomic) NSInteger parkingId;
@property (strong, nonatomic) NSString *location;
@property (assign, nonatomic) NSInteger totalSlots;
@property (assign, nonatomic) NSInteger referenceId;
@property (assign, nonatomic) float longitude;
@property (assign, nonatomic) float latitude;
@property (assign, nonatomic) NSInteger occupiedSlots;
@property (strong, nonatomic) NSString *operatingHours;
@property (strong, nonatomic) NSString *hourlyRate;
@property (strong, nonatomic) NSString *contactDetails;
@property (strong, nonatomic) NSString *type;
@property (assign, nonatomic) NSInteger merchantPartnerId;

@property (assign, nonatomic) long earlyBirdFrom;
@property (assign, nonatomic) long earlyBirdTo;

@property (strong, nonatomic) NSString *distance;

- (id)initWithParkingLotDictionary:(NSDictionary *)dictionary;
- (id)initWithTransactionModel:(TransactionModel *)model;

@end
