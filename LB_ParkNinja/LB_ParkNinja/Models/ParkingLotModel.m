////
//  ParkingLotModel.m
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 11/25/15.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import "ParkingLotModel.h"

@implementation ParkingLotModel

- (id)initWithParkingLotDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    
    if (self)
    {
        self.parkingId = [[dictionary objectForKey:@"id"] integerValue];
        self.location = [dictionary objectForKey:@"location"];
        self.totalSlots = [[dictionary objectForKey:@"location"] integerValue];
        self.referenceId = [[dictionary objectForKey:@"refId"] integerValue];
        self.latitude = [[dictionary objectForKey:@"latitude"] floatValue];
        
        self.longitude = [[dictionary objectForKey:@"longitude"] floatValue];
        
        self.occupiedSlots = [[dictionary objectForKey:@"occupiedSlots"] integerValue];
        
        self.operatingHours = [dictionary objectForKey:@"operatingHours"];
        self.hourlyRate = [dictionary objectForKey:@"hourlyRate"];
        self.contactDetails = [dictionary objectForKey:@"contactDetails"];
        
        self.type = [dictionary objectForKey:@"type"];
       
        self.merchantPartnerId = [[dictionary objectForKey:@"merchantPartnerId"] integerValue];
        
        self.earlyBirdFrom = [[dictionary objectForKey:@"earlyBirdFrom"] longValue];
        self.earlyBirdTo = [[dictionary objectForKey:@"earlyBirdTo"] longValue];

    }
    
    return self;
}

- (id)initWithTransactionModel:(TransactionModel *)model

{
    self = [super init];
    
    if (self)
    {
        self.location = model.parkingLocation;
        self.merchantPartnerId = model.merchantPartnerId;
    }
    
    return self;
}

@end
