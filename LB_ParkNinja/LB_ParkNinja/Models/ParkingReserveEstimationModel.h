//
//  ParkingReserveEstimationModel.h
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 1/4/16.
//  Copyright © 2016 Tricia Rullan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TransactionModel.h"

@interface ParkingReserveEstimationModel : NSObject

@property (assign, nonatomic) long reserveFrom;
@property (assign, nonatomic) long reserveTo;
@property (strong, nonatomic) NSString *email;
@property (assign, nonatomic) BOOL earlyBird;
@property (strong, nonatomic) NSString *promoCode;

@property (assign, nonatomic) NSInteger estimateAmount;
@property (assign, nonatomic) NSInteger verifyAmount;

@property (assign, nonatomic) NSInteger mobileNum;

- (id)initParkingEstimationWithDictionary:(NSDictionary *)dictionary;
- (id)initParkingEstimationWithTransactionModel:(TransactionModel *)model;

@end
