//
//  LoginFBUserModel.h
//  LB_ParkNinja
//
//  Created by Tric Rullan on 10/11/2015.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

#import "User.h"

@interface UserModel : NSObject

@property (strong, nonatomic) NSString *userId;
@property (strong, nonatomic) NSString *activationKey;
@property (strong, nonatomic) NSString *birthday;
@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSString *facebookId;
@property (strong, nonatomic) NSString *firstName;
@property (strong, nonatomic) NSString *middleName;
@property (strong, nonatomic) NSString *lastName;
@property (strong, nonatomic) NSString *nameSuffix;
@property (strong, nonatomic) NSString *fullName;
@property (strong, nonatomic) NSString *emailNew;
@property (strong, nonatomic) NSString *passwordNew;
@property (strong, nonatomic) NSString *password;
@property (strong, nonatomic) NSString *roleId;
@property (strong, nonatomic) NSString *roles;
@property (strong, nonatomic) NSString *status;
@property (strong, nonatomic) NSString *userStatus;
@property (strong, nonatomic) NSString *userName;
@property (strong, nonatomic) NSString *mobileNo;

@property (strong, nonatomic) NSData *displayImage;

- (id)initWithUserInfoDictionary:(NSDictionary *)dictionary;
- (id)initWithUserModel:(User *)user;
- (void)initWithUserProfileImageData:(User *)user;

@end
