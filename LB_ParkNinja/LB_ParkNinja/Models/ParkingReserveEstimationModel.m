//
//  ParkingReserveEstimationModel.m
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 1/4/16.
//  Copyright © 2016 Tricia Rullan. All rights reserved.
//

#import "ParkingReserveEstimationModel.h"

@implementation ParkingReserveEstimationModel

- (id)initParkingEstimationWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];

    if (self)
    {
        self.email = [dictionary objectForKey:@"email"];
        
        self.reserveFrom = [[dictionary objectForKey:@"reserveFrom"] longValue];
        self.reserveTo = [[dictionary objectForKey:@"reserveTo"] longValue];
        
        self.estimateAmount = [[dictionary objectForKey:@"estimateAmount"] integerValue];
        self.verifyAmount = [[dictionary objectForKey:@"verifyAmount"] integerValue];
        
        self.earlyBird = [[dictionary objectForKey:@"earlyBird"] boolValue];
    }
    
    return self;
}

- (id)initParkingEstimationWithTransactionModel:(TransactionModel *)model
{
    self = [super init];
    
    if (self)
    {
        self.reserveFrom = model.reserveFrom;
        self.reserveTo = model.reserveTo;
        
        self.estimateAmount = model.estimateAmount;
    }
    
    return self;
}

@end
