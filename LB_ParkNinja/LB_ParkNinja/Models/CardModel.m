//
//  CardModel.m
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 1/8/16.
//  Copyright © 2016 Tricia Rullan. All rights reserved.
//

#import "CardModel.h"

@implementation CardModel

- (id)initCardModelWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    
    if (self)
    {
        self.cardId = [[dictionary objectForKey:@"cardId"] integerValue];
       // self.status = [[dictionary objectForKey:@"cardStatus"] boolValue];
        
        self.maskedNum = [dictionary objectForKey:@"cardType"];
        self.type = [dictionary objectForKey:@"maskedCardNo"];
        //self.token = [dictionary objectForKey:@"token"];
    }
    
    return self;
}

@end
