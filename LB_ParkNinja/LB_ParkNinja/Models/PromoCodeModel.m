//
//  PromoCodeModel.m
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 1/9/16.
//  Copyright © 2016 Tricia Rullan. All rights reserved.
//

#import "PromoCodeModel.h"

@implementation PromoCodeModel

- (id)initPromoCodeWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    
    if (self)
    {
        self.promoCodeId = [[dictionary objectForKey:@"id"] integerValue];
        self.code = [dictionary objectForKey:@"promoCode"];
        self.discountRateCurrency = [[dictionary objectForKey:@"discountRateCurrency"] integerValue];
        self.discountRatePercentage = [[dictionary objectForKey:@"discountRatePercentage"] integerValue];
        self.usesPerUser = [[dictionary objectForKey:@"usesPerUser"] integerValue];
        
        self.durationFrom = [[dictionary objectForKey:@"usesPerUser"] longValue];
        self.durationTo = [[dictionary objectForKey:@"usesPerUser"] longValue];
    }
    
    return self;

}

@end
