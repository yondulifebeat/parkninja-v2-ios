//
//  PromoCodeModel.h
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 1/9/16.
//  Copyright © 2016 Tricia Rullan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PromoCodeModel : NSObject

@property (assign, nonatomic) NSInteger promoCodeId;
@property (strong, nonatomic) NSString *codeParkingLot;
@property (strong, nonatomic) NSString *code;
@property (assign, nonatomic) NSInteger discountRateCurrency;
@property (assign, nonatomic) NSInteger discountRatePercentage;
@property (assign, nonatomic) NSInteger usesPerUser;
@property (assign, nonatomic) long durationFrom;
@property (assign, nonatomic) long durationTo;
@property (strong, nonatomic) NSString *deletedCodeParkingLots;

- (id)initPromoCodeWithDictionary:(NSDictionary *)dictionary;

@end
