//
//  LoginFBUserModel.m
//  LB_ParkNinja
//
//  Created by Tric Rullan on 10/11/2015.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import "UserModel.h"

@implementation UserModel

- (id)initWithUserInfoDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    
    if (self)
    {
        self.userId = [dictionary objectForKey:@"userId"];
        self.activationKey = [dictionary objectForKey:@"activationKey"];
        self.birthday = [dictionary objectForKey:@"birthday"];
        self.email = [dictionary objectForKey:@"email"];
        self.facebookId = [dictionary objectForKey:@"facebookId"];
        
        self.firstName = [dictionary objectForKey:@"firstName"];
        self.middleName = [dictionary objectForKey:@"middleName"];
        self.lastName = [dictionary objectForKey:@"lastName"];
        self.nameSuffix = [dictionary objectForKey:@"nameSuffix"];
        self.fullName = [dictionary objectForKey:@"fullName"];
        
        self.emailNew = [dictionary objectForKey:@"newEmail"];
        
        self.password = [dictionary objectForKey:@"password"];
        self.passwordNew = [dictionary objectForKey:@"newPassword"];
        self.roleId = [dictionary objectForKey:@"roleId"];
        self.roles = [dictionary objectForKey:@"roles"];
        self.status = [dictionary objectForKey:@"status"];
        self.userStatus = [dictionary objectForKey:@"userStatus"];
        self.userName = [dictionary objectForKey:@"username"];
        self.mobileNo = [dictionary objectForKey:@"mobileNo"];
        
    }
    
    return self;
}

- (id)initWithUserModel:(User *)user
{
    self = [super init];
    
    if (self)
    {
        self.userId = user.userId;
        self.birthday = user.birthday;
        self.email = user.email;
        self.firstName = user.firstName;
        self.lastName = user.lastName;
        self.middleName = user.middleName;
        self.fullName = [NSString stringWithFormat:@"%@ %@ %@", self.firstName, self.middleName, self.lastName];
        self.mobileNo = user.mobileNo;
        self.userName = user.userName;
        self.userStatus = user.userStatus;
    }
    
    return self;
}

- (void)initWithUserProfileImageData:(User *)user
{

    self.displayImage = user.displayImage;
}
    
@end
