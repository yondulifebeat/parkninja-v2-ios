//
//  LoginVC.m
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 11/5/15.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import "LoginVC.h"
#import <MBProgressHUD/MBProgressHUD.h>

#import "HttpManagerBlocks.h"
#import "GlobalConstants.h"
#import "GlobalMethods.h"
#import "UserLoginModel.h"
#import "UserModel.h"
#import "HomeVC.h"

typedef NS_ENUM(NSInteger, LOGIN_TEXTFIELD_INDEX)
{
    TF_INDEX_EMAIL,
    TF_INDEX_PASSWORD
};

@interface LoginVC ()

@property (weak, nonatomic) IBOutlet UITableView *loginTableView;
@property (strong, nonatomic) UserLoginModel *userLoginModel;
@property (strong, nonatomic) UserModel *userModel;

@end

@implementation LoginVC

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self initModels];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private Methods
- (void)initializeItems
{
    [[UINavigationBar appearance] setHidden:NO];
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"bg_splash"] forBarMetrics:UIBarMetricsDefault];
    
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)initModels
{
    self.userLoginModel = [[UserLoginModel alloc] init];
    self.userModel = [[UserModel alloc] init];
}


- (void)loginButton:(FBSDKLoginButton *)loginButton didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result error:(NSError *)error
{
    if ([FBSDKAccessToken currentAccessToken])
    {
        NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
        [parameters setValue:@"id, email, first_name, last_name, picture.type(large)" forKey:@"fields"];
        
        FBSDKGraphRequest *request =[[FBSDKGraphRequest alloc]initWithGraphPath:@"me" parameters:parameters];
        [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                              id result,
                                              NSError *error)
         {
             // Handle the result
             NSLog(@"%@",result);
             
             NSString *profURL = [[[result objectForKey:@"picture"] objectForKey:@"data"] objectForKey:@"url"];
             
             NSDictionary *userInfo = [[NSDictionary alloc] initWithObjectsAndKeys:[result objectForKey:@"email"], @"email", [result objectForKey:@"id"], @"facebookId", nil];
             [self requestUserFBDetails:userInfo lastName:[result objectForKey:@"last_name"] firstName:[result objectForKey:@"first_name"] profileURL:profURL];
         }];
    }
}

- (void)loginButtonDidLogOut:(FBSDKLoginButton *)loginButton
{
    FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
    [loginManager logOut];
    
    [FBSDKAccessToken setCurrentAccessToken:nil];
}

- (void)showForgotPasswordPopUp
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Forgot Password"
                                                                   message:@"We will send you an email to reset your password."
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *done = [UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleDefault
                                                 handler:^(UIAlertAction *action) {
                                                
                                                     NSString *email = ((UITextField *)[alert.textFields objectAtIndex:0]).text;
                                                     [self requestForgotPasswordWithEmail:email];
                                                     
                                                   
                                               }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction *action) {
                                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                                   }];
    
    [alert addAction:cancel];
    [alert addAction:done];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Email";
    }];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)showActivationPopUp
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Enter Actication Code"
                                                                   message:@"Please enter the activation code we sent to your email."
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *done = [UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleDefault
                                                 handler:^(UIAlertAction *action) {
                                                     
                                                     NSString *code = ((UITextField *)[alert.textFields objectAtIndex:0]).text;
                                                     
                                                     NSLog(@"ACTIVATION CODE: %@", code);
                                                     
                                                     [self requestActivateAccountWithCode:code];
                                                 }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction *action) {
                                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                                   }];
    
    [alert addAction:cancel];
    [alert addAction:done];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField)
     {
         textField.placeholder = @"Activation Code";
     }];
    
    [self presentViewController:alert animated:YES completion:nil];

}

#pragma mark - UIButtonAction
- (IBAction)dismissViewController:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - API Methods
- (void)requestUserFBDetails:(NSDictionary *)userInfo lastName:(NSString *)last firstName:(NSString *)first profileURL:(NSString *)pathURL
{
    MBProgressHUD *HUD = [MBProgressHUD showHUDAddedTo:[[UIApplication sharedApplication] keyWindow] animated:YES];
    HUD.dimBackground = YES;
    HUD.labelText = @"Loading";
    
    NSLog(@"USERINFO: %@", userInfo);
    
    [[HttpManagerBlocks sharedInstance] postToUrl:URL_LOGIN_FACEBOOK withParameters:userInfo withCompletionBlockSuccess:^(NSDictionary *responseDictionary, RESPONSE_CODE responseCode){
        
        UserModel *model = [[UserModel alloc] initWithUserInfoDictionary:responseDictionary];
        
        NSDictionary *userDict = [[NSDictionary alloc] initWithObjectsAndKeys:model.email, @"email", first, @"firstName", last, @"lastName", @"", @"middleName", @"", @"nameSuffix", @"", @"mobileNo", [NSNumber numberWithLong:0], @"birthday", nil];
        
         NSLog(@"USERINFO: %@", userDict);
        
        [GlobalMethods saveUserWithInfo:userDict];
        
        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:pathURL]];
        [GlobalMethods saveUserWithImage: [UIImage imageWithData:imageData]];
         
        AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [app setRootViewControllerWithSB:SB_MAIN];

         [MBProgressHUD hideAllHUDsForView:[[UIApplication sharedApplication] keyWindow] animated:YES];
        
    } failure:^(NSError *error, NSString *customDescription, NSString *serverError) {
        
        UIAlertController *alert = [GlobalMethods showAlertViewWithTitle:nil message:serverError];
        [self presentViewController:alert animated:YES completion:nil];
        
        [MBProgressHUD hideAllHUDsForView:[[UIApplication sharedApplication] keyWindow] animated:YES];
    }];
}

- (void)requestLoginUserDetails
{
    MBProgressHUD *HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.dimBackground = YES;
    HUD.labelText = @"Loading";
    
    NSDictionary *userInfo = [[NSDictionary alloc] initWithObjectsAndKeys:self.userLoginModel.email, @"email", self.userLoginModel.password, @"password", nil];
    
    NSLog(@"USERINFO: %@", userInfo);
    
    [[HttpManagerBlocks sharedInstance] postToUrl:URL_LOGIN withParameters:userInfo withCompletionBlockSuccess:^(NSDictionary *responseDictionary, RESPONSE_CODE responseCode){
        
        NSLog(@"RESPONSE: %@", responseDictionary);
        
        self.userModel = [[UserModel alloc] initWithUserInfoDictionary:responseDictionary];
        
        if ([self.userModel.userStatus isEqualToString:@"NEW"])
        {
            [self showActivationPopUp];
        }
        
        else
        {
            [GlobalMethods saveUserWithInfo:responseDictionary];
            
            AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            [app setRootViewControllerWithSB:SB_MAIN];
        }
        
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
    } failure:^(NSError *error, NSString *customDescription, NSString *serverError) {
        

        UIAlertController *alert =  [GlobalMethods showAlertViewWithTitle:@"Login Error" message:serverError];
        [self presentViewController:alert animated:YES completion:nil];
        
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
    }];
}

- (void)requestActivateAccountWithCode:(NSString *)code
{
    MBProgressHUD *HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.dimBackground = YES;
    HUD.labelText = @"Loading";
    
    NSDictionary *activateDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:self.userModel.email, @"email", code, @"activationKey", nil];
    
    [[HttpManagerBlocks sharedInstance] postToUrl:URL_ACTIVATE_ACCOUNT withParameters:activateDictionary withCompletionBlockSuccess:^(NSDictionary *responseDictionary, RESPONSE_CODE responseCode){
        
        self.userModel = [[UserModel alloc] initWithUserInfoDictionary:responseDictionary];
        
        [GlobalMethods saveUserWithInfo:responseDictionary];
        
        AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [app setRootViewControllerWithSB:SB_MAIN];
        
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
    } failure:^(NSError *error, NSString *customDescription, NSString *serverError) {
        
        UIAlertController *alert =  [GlobalMethods showAlertViewWithTitle:@"Login Error" message:serverError];
        [self presentViewController:alert animated:YES completion:nil];

        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    }];
}

- (void)requestForgotPasswordWithEmail:(NSString *)email
{
    MBProgressHUD *HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.dimBackground = YES;
    HUD.labelText = @"Loading";
    
    NSDictionary *activateDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:email, @"email", nil];
    
    [[HttpManagerBlocks sharedInstance] postToUrl:URL_FORGOT_PASSWORD withParameters:activateDictionary withCompletionBlockSuccess:^(NSDictionary *responseDictionary, RESPONSE_CODE responseCode){
        
        UIAlertController *alert =  [GlobalMethods showAlertViewWithTitle:@"Reset Password" message:@"New password sent to your email."];
        [self presentViewController:alert animated:YES completion:nil];
        
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
    } failure:^(NSError *error, NSString *customDescription, NSString *serverError) {
        
        UIAlertController *alert =  [GlobalMethods showAlertViewWithTitle:@"Login Error" message:serverError];
        [self presentViewController:alert animated:YES completion:nil];
        
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    }];
}

#pragma mark - LoginDetailsCell
- (LoginDetailsCell *)setLoginDetailsWithRow:(NSInteger)row
{
    LoginDetailsCell *cell = [self.loginTableView dequeueReusableCellWithIdentifier:@"LoginDetailsCell"];
    
    if (cell == nil)
    {
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"LoginDetailsCell" owner:self options:nil];
        cell = (LoginDetailsCell *)nibArray[0];
    }
    
    cell.delegate = self;
    cell.facebookLoginButton.readPermissions = @[@"public_profile", @"email"];
    cell.facebookLoginButton.delegate = self;

    return cell;
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self setLoginDetailsWithRow:indexPath.row];
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 320;
}

#pragma mark - LoginDetailsCellDelegate
- (void)getUserEmail:(NSString *)email andPassword:(NSString *)password
{

}

- (void)buttonActionWithTag:(NSInteger)tag
{
    switch (tag)
    {
        case LOGIN_BTN_ACTION_FACEBOOK:
        {
          //  [self checkFacebookSession];
            break;
        }
            
        case LOGIN_BTN_ACTION_MANUAL:
        {
            [self.view endEditing:YES];
            [self requestLoginUserDetails];
            break;
        }
        case LOGIN_BTN_FORGOT_PASSWORD:
        {
            [self showForgotPasswordPopUp];
            break;
        }
    }
    
}

#pragma mark - UITextFieldDelegate
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = _loginTableView.frame;
    if (movedUp)
    {
        rect.size.height -= kKEYBOARD_HEIGHT;
    }
    else
    {
        rect.size.height += kKEYBOARD_HEIGHT;
    }
    
    _loginTableView.frame = rect;
    
    [UIView commitAnimations];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{

    [self setViewMovedUp:YES];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        NSIndexPath *rowIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        [_loginTableView scrollToRowAtIndexPath:rowIndexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
    });

}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self setViewMovedUp:NO];
    
    NSString *userInput = textField.text;
    
    switch (textField.tag)
    {
        case TF_INDEX_EMAIL:
        {
            self.userLoginModel.email = userInput;
            break;
        }
            
        case TF_INDEX_PASSWORD:
        {
            self.userLoginModel.password = userInput;
            break;
        }
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [textField resignFirstResponder];
    }
    return NO; // We do not want UITextField to insert line-breaks.
}

@end
