//
//  SignUpDetailsCell.m
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 11/12/15.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import "SignUpDetailsCell.h"

#import <CoreText/CoreText.h>

@implementation SignUpDetailsCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)initDetails
{
    NSMutableAttributedString *preString = [[NSMutableAttributedString alloc] initWithString:@"By proceeding, you also agree to "];
    
    NSMutableAttributedString *parkNinja = [[NSMutableAttributedString alloc] initWithString:@"Park  Ninja's "];

   [parkNinja addAttribute:(NSString *)kCTFontAttributeName value:[UIFont boldSystemFontOfSize:17] range:(NSRange){0,[parkNinja length]}];
    
    NSMutableAttributedString *termsString = [[NSMutableAttributedString alloc] initWithString:@"Terms of Service and Privacy Policy"];
    [termsString addAttribute:(NSString *)kCTUnderlineStyleAttributeName
                      value:[NSNumber numberWithInt:kCTUnderlineStyleSingle]
                      range:(NSRange){0,[termsString length]}];
    
    
    NSMutableAttributedString *comString = [[NSMutableAttributedString alloc] initWithAttributedString:preString];
    [comString appendAttributedString:parkNinja];
    [comString appendAttributedString:termsString];
    
    self.termsAndConditionLabel.attributedText = comString;
    self.termsAndConditionLabel.textColor = [UIColor blackColor];
    
    
    //add gesture recognizer to label
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(termsAndConditionsLabelTapped)];
    
    [self.termsAndConditionLabel addGestureRecognizer:singleTap];
}

- (void)setSignUpDetailsWithModel:(UserSignUpModel *)model
{
    self.firstNameTextField.text = model.firstName;
    self.middleInitialTextField.text = model.middleInitial;
    self.lastNameTextField.text = model.lastName;
    self.nameSuffixTextField.text = model.nameSuffix;
    self.emailTextField.text = model.email;
    self.passwordTextField.text = model.password;
    self.confirmPasswordTextField.text = model.confirmPassword;
    self.mobileNumTextField.text = model.mobileNum;    
}

- (void)termsAndConditionsLabelTapped
{
    [self.delegate showTermsAndConditionsPage];
}

@end
