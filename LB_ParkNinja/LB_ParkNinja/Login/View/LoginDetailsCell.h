//
//  LoginDetailsCell.h
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 11/12/15.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>

@protocol LoginDetailsCellDelegate;

@interface LoginDetailsCell : UITableViewCell

@property (assign, nonatomic) id <LoginDetailsCellDelegate> delegate;

@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet FBSDKLoginButton *facebookLoginButton;

@end

@protocol LoginDetailsCellDelegate <NSObject>
@required

- (void)getUserEmail:(NSString *)email andPassword:(NSString *)password;
- (void)buttonActionWithTag:(NSInteger)tag;

@end;