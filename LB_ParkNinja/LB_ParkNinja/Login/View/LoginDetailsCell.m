//
//  LoginDetailsCell.m
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 11/12/15.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import "LoginDetailsCell.h"

#import "GlobalConstants.h"


@implementation LoginDetailsCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)resignAllTextfields
{
    [self.emailTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
}

#pragma mark - UIButtonAction
- (IBAction)facebookLogin:(id)sender
{
    [self.delegate buttonActionWithTag:LOGIN_BTN_ACTION_FACEBOOK];
    [self resignAllTextfields];
}

- (IBAction)login:(id)sender
{
    [self.delegate buttonActionWithTag:LOGIN_BTN_ACTION_MANUAL];
    [self resignAllTextfields];
}

- (IBAction)forgotPassword:(id)sender
{
    [self.delegate buttonActionWithTag:LOGIN_BTN_FORGOT_PASSWORD];
    [self resignAllTextfields];
}

@end
