//
//  SignUpVC.m
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 11/5/15.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import "SignUpVC.h"
#import <MBProgressHUD/MBProgressHUD.h>

#import "UserSignUpModel.h"
#import "HttpManagerBlocks.h"
#import "GlobalConstants.h"
#import "GlobalMethods.h"
#import "UserModel.h"

#import "TermsAndConditionsVC.h"
#import "HomeVC.h"

#import "AppDelegate.h"

#define MAX_LENGTH 11
#define MAX_LENGTH_SURNAME 2

typedef NS_ENUM(NSInteger, SIGN_UP_TEXTFIELD_INDEX)
{
    TF_INDEX_FIRST_NAME,
    TF_INDEX_MIDDLE_INITIAL,
    TF_INDEX_LAST_NAME,
    TF_INDEX_NAME_SUFFIX,
    TF_INDEX_EMAIL,
    TF_INDEX_PASSWORD,
    TF_INDEX_CONFIRM_PASSWORD,
    TF_INDEX_MOBILE_NUM,
    TF_INDEX_BIRTHDAY
};

@interface SignUpVC ()

@property (weak, nonatomic) IBOutlet UITableView *signUpTableView;
@property (strong, nonatomic) UserSignUpModel *userModel;
@property (strong, nonatomic) IBOutlet UIDatePicker *datePicker;

@property (strong, nonatomic) IBOutlet UIView *pickerContainerView;

@end

@implementation SignUpVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
    if (self.userModel == nil)
        self.userModel = [[UserSignUpModel alloc] init];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private Methods
- (void)showActivationPopUp
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Enter Actication Code"
                                                                   message:@"Please enter the activation code we sent to your email."
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *done = [UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleDefault
                                                 handler:^(UIAlertAction *action) {
                                                     
                                                     NSString *code = ((UITextField *)[alert.textFields objectAtIndex:0]).text;
                                                     
                                                     NSLog(@"ACTIVATION CODE: %@", code);
                                                     
                                                     [self requestActivateAccountWithCode:code];
                                                 }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction *action) {
                                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                                   }];
    
    [alert addAction:cancel];
    [alert addAction:done];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField)
     {
         textField.placeholder = @"Activation Code";
     }];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (void)showDatePickerView
{
    [self.view endEditing:YES];
    
    self.pickerContainerView.hidden = NO;
}

- (void)resignAllTextFields
{
    [self.view endEditing:YES];
}

- (BOOL)checkAllUserInput
{
    BOOL isValid = YES;
    
    NSString *message = MESSAGE_FIELDS_INCOMPLETE;
    
    if ((![GlobalMethods checkInputForWhiteSpaces:self.userModel.firstName] || ![GlobalMethods checkInputForWhiteSpaces:self.userModel.middleInitial] ||
         ![GlobalMethods checkInputForWhiteSpaces:self.userModel.lastName] || ![GlobalMethods checkInputForWhiteSpaces:self.userModel.email]||
         ![GlobalMethods checkInputForWhiteSpaces:self.userModel.password] || ![GlobalMethods checkInputForWhiteSpaces:self.userModel.confirmPassword] ||
         ![GlobalMethods checkInputForWhiteSpaces:self.userModel.mobileNum]))
    {
        isValid = NO;
    }
    
    else if (![GlobalMethods NSStringIsValidEmail:self.userModel.email])
    {
        message = @"Invalid email";
        isValid = NO;
    }
    
    else if (![self.userModel.password isEqualToString:self.userModel.confirmPassword])
    {
        message = @"Password doesn't match.";
        isValid = NO;
    }
    
    UIAlertController *alert = [GlobalMethods showAlertViewWithTitle:@"Sign Up" message:message];
    
    if (!isValid)
        [self presentViewController:alert animated:YES completion:nil];
    
    return isValid;
}

- (void)signUpUserDetails
{
    [self resignAllTextFields];
    
    if ([self checkAllUserInput] && [GlobalMethods isConnected])
    {
        [self requestUserDetails];
    };
}

#pragma mark - UIDatePicker
- (IBAction)datePickerAction:(id)sender
{
   // NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //[dateFormatter setDateFormat:@"dd-MM-yyyy HH:mm"];
}

#pragma mark - API Request
- (void)requestActivateAccountWithCode:(NSString *)code
{
    MBProgressHUD *HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.dimBackground = YES;
    HUD.labelText = @"Loading";
    
    NSDictionary *activateDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:self.userModel.email, @"email", code, @"activationKey", nil];
    
    [[HttpManagerBlocks sharedInstance] postToUrl:URL_ACTIVATE_ACCOUNT withParameters:activateDictionary withCompletionBlockSuccess:^(NSDictionary *responseDictionary, RESPONSE_CODE responseCode){
        
        [GlobalMethods saveUserWithInfo:responseDictionary];
        
        AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [app setRootViewControllerWithSB:SB_MAIN];
        
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
    } failure:^(NSError *error, NSString *customDescription, NSString *serverError) {

        UIAlertController *alert =  [GlobalMethods showAlertViewWithTitle:@"Login Error" message:serverError];
        [self presentViewController:alert animated:YES completion:nil];
        
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    }];
}

- (void)requestUserDetails
{
    MBProgressHUD *HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.dimBackground = YES;
    HUD.labelText = @"Loading";
    
    if ([self.userModel.nameSuffix length] == 0)
        self.userModel.nameSuffix = @"";
    
    NSDictionary *userInfo = [[NSDictionary alloc] initWithObjectsAndKeys:self.userModel.email, @"email", self.userModel.password, @"password", self.userModel.firstName, @"firstName", self.userModel.lastName, @"lastName",  self.userModel.middleInitial, @"middleName", self.userModel.nameSuffix, @"nameSuffix",  self.userModel.mobileNum, @"mobileNo", [NSNumber numberWithInteger:self.userModel.birthday], @"birthday", nil];
   
    NSLog(@"USER INFO: %@", userInfo);
    
    [[HttpManagerBlocks sharedInstance] postToUrl:URL_SIGN_UP withParameters:userInfo withCompletionBlockSuccess:^(NSDictionary *responseDictionary, RESPONSE_CODE responseCode){
        
        [self showActivationPopUp];
        
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
    } failure:^(NSError *error, NSString *customDescription, NSString *serverError) {

        UIAlertController *alert =  [GlobalMethods showAlertViewWithTitle:@"Sign Up Error" message:serverError];
        [self presentViewController:alert animated:YES completion:nil];
        
         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    }];
}

#pragma mark - UIButtonAction
- (IBAction)dismissViewController:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)dismissPickerView:(id)sender
{
    NSDate *today = [NSDate date]; // it will give you current date
    NSComparisonResult result;
    //has three possible values: NSOrderedSame,NSOrderedDescending, NSOrderedAscending
    
    result = [today compare:self.datePicker.date]; // comparing two dates
    
    if (result == NSOrderedAscending)
    {
        UIAlertController *alert = [GlobalMethods showAlertViewWithTitle:@"Invalid Date" message:@"Kindly check your input date."];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    else
    {
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setTimeZone:[NSTimeZone timeZoneWithName:@"PHT"]];
        [dateFormat setDateFormat:@"MM/dd/yyyy"];
        NSString *dateString = [dateFormat stringFromDate:self.datePicker.date];
        NSString *stringDate = [NSString stringWithFormat:@"%@", dateString];
        
        NSDateFormatter *dateFormat1 = [[NSDateFormatter alloc] init];
        [dateFormat1 setTimeZone:[NSTimeZone timeZoneWithName:@"PHT"]];
        [dateFormat1 setDateFormat:@"MM/dd/yyyy"];
        NSDate *selectedDate = [dateFormat1 dateFromString:stringDate];
        NSLog(@"PICKER DATE: %@", selectedDate);
        
        NSCalendar *calendar = [[NSCalendar alloc]
                                initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        NSDateComponents *components = [[NSDateComponents alloc] init];
        [components setHour:8];
        
        NSDate *dateInput = [calendar dateByAddingComponents:components toDate:selectedDate options:0];
        
        long timestamp = ([dateInput timeIntervalSince1970] * 1000) + 14400000;
        
        self.userModel.birthday = timestamp;
        
        [self.signUpTableView reloadData];
        self.pickerContainerView.hidden = YES;
    }
}

- (IBAction)cancelPickerView:(id)sender
{
    self.pickerContainerView.hidden = YES;
}

#pragma mark - UITableViewCell
- (SignUpDetailsCell *)setSignUpDetailsWithRow:(NSInteger)row
{
    SignUpDetailsCell *cell = [self.signUpTableView dequeueReusableCellWithIdentifier:@"SignUpDetailsCell"];
    
    if (cell == nil)
    {
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"SignUpDetailsCell" owner:self options:nil];
        cell = (SignUpDetailsCell *)nibArray[0];
    }
    
    cell.delegate = self;
    [cell initDetails];
    [cell.signUpButton addTarget:self action:@selector(signUpUserDetails) forControlEvents:UIControlEventTouchUpInside];
    [cell.birthdayButton addTarget:self action:@selector(showDatePickerView) forControlEvents:UIControlEventTouchUpInside];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM-dd-yyyy"];
    
    NSString *formattedDate = [dateFormatter stringFromDate:self.datePicker.date];
    
    [cell.birthdayButton setTitle:formattedDate forState: UIControlStateNormal];
    
    [cell setSignUpDetailsWithModel:self.userModel];
    
    return cell;
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self setSignUpDetailsWithRow:indexPath.row];
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 460;
}


#pragma mark - UITextFieldDelegate
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = _signUpTableView.frame;
    if (movedUp)
    {
        rect.size.height -= kKEYBOARD_HEIGHT;
    }
    else
    {
        rect.size.height += kKEYBOARD_HEIGHT;
    }
    
    _signUpTableView.frame = rect;
    
    [UIView commitAnimations];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self setViewMovedUp:YES];
    
    NSInteger position = UITableViewScrollPositionNone;
    
    switch (textField.tag)
    {
        case TF_INDEX_FIRST_NAME:
        case TF_INDEX_MIDDLE_INITIAL:
        case TF_INDEX_LAST_NAME:
        case TF_INDEX_NAME_SUFFIX:
        {
            position = UITableViewScrollPositionTop;
            break;
        }
            
        case TF_INDEX_EMAIL:
        case TF_INDEX_PASSWORD:
        case TF_INDEX_CONFIRM_PASSWORD:
        {
            position = UITableViewScrollPositionMiddle;
            break;
        }
            
        case TF_INDEX_MOBILE_NUM:
        case TF_INDEX_BIRTHDAY:
        {
            position = UITableViewScrollPositionBottom;
            break;
        }
    }

    dispatch_async(dispatch_get_main_queue(), ^{
        NSIndexPath *rowIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        [_signUpTableView scrollToRowAtIndexPath:rowIndexPath atScrollPosition:position animated:YES];
    });
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self setViewMovedUp:NO];
    
    NSString *userInput = textField.text;
    
    switch (textField.tag)
    {
        case TF_INDEX_FIRST_NAME:
        {
            self.userModel.firstName = userInput;
            break;
        }
            
        case TF_INDEX_MIDDLE_INITIAL:
        {
            self.userModel.middleInitial = userInput;
            break;
        }
            
        case TF_INDEX_LAST_NAME:
        {
            self.userModel.lastName = userInput;
            break;
        }
            
        case TF_INDEX_NAME_SUFFIX:
        {
            self.userModel.nameSuffix = userInput;
            break;
        }
            
        case TF_INDEX_EMAIL:
        {
            self.userModel.email = userInput;
            break;
        }
            
        case TF_INDEX_PASSWORD:
        {
            self.userModel.password = userInput;
            break;
        }
            
        case TF_INDEX_CONFIRM_PASSWORD:
        {
            self.userModel.confirmPassword = userInput;
            break;
        }
            
        case TF_INDEX_MOBILE_NUM:
        {
            self.userModel.mobileNum = userInput;
            break;
        }
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField.tag == TF_INDEX_MOBILE_NUM)
    {
        if (textField.text.length >= MAX_LENGTH && range.length == 0)
            return NO;
        
        else
            return YES;
    }
    else if (textField.tag == TF_INDEX_NAME_SUFFIX)
    {
        if (textField.text.length >= MAX_LENGTH_SURNAME && range.length == 0)
            return NO;
    
        else
            return YES;
    
    }
    
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [textField resignFirstResponder];
    }
    return NO; // We do not want UITextField to insert line-breaks.
}

#pragma mark - SignUpDetailsCellDelegate
- (void)showTermsAndConditionsPage
{
    UIStoryboard *loginSB = [UIStoryboard storyboardWithName:@"LoginSB" bundle:nil];
    TermsAndConditionsVC *vc = [loginSB instantiateViewControllerWithIdentifier:@"TermsAndConditionsVC"];
    [self.navigationController pushViewController:vc animated:YES];

}

@end
