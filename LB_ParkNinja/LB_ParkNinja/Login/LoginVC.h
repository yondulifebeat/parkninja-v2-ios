//
//  LoginVC.h
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 11/5/15.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LoginDetailsCell.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>

@interface LoginVC : UIViewController <LoginDetailsCellDelegate, FBSDKLoginButtonDelegate>

@end
