//
//  HttpManagerBlocks.h
//  Blocks
//
//  Created by Mailyn Sajorda on 3/18/15.
//  Copyright (c) 2015 egg.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

@interface HttpManagerBlocks : NSObject

typedef enum {
    RESPONSE_SUCCESS = 0,
    RESPONSE_GENERIC_ERROR,
    RESPONSE_METHOD_NOT_FOUND,
    RESPONSE_BAD_REQUEST
} RESPONSE_CODE;

+ (HttpManagerBlocks *)sharedInstance;

typedef void(^imageSuccess)(UIImage *imageResponse);

typedef void(^timeSuccess)(NSString *timeResponse);

typedef void(^success)(NSDictionary *responseDictionary, RESPONSE_CODE responseCode);
typedef void(^failure)(NSError *error, NSString *customDescription, NSString *serverError);

- (void)getTimeFromUrl:(NSString *)urlStr withParameters:(NSDictionary *)parameters withCompletionBlockSuccess:(timeSuccess)success failure:(failure)failure;

- (void)getFromUrl:(NSString *)urlStr withParameters:(NSDictionary *)parameters withCompletionBlockSuccess:(success)success
           failure:(failure)failure;

- (void)postToUrl:(NSString *)urlStr withParameters:(NSDictionary *)parameters withCompletionBlockSuccess:(success)success
          failure:(failure)failure;

- (void)postPurchaseParkingToUrl:(NSString *)urlStr withParameters:(NSDictionary *)parameters withCompletionBlockSuccess:(timeSuccess)success failure:(failure)failure;

- (void)postQRCodeToUrl:(NSString *)urlStr withParameters:(NSDictionary *)parameters withCompletionBlockSuccess:(imageSuccess)success failure:(failure)failure;

- (void)postMultipartRequestForImage:(UIImage *)image withFilename:(NSString *)filename toUrl:(NSString *)urlStr withParameters:(NSDictionary *)parameters withCompletionBlockSuccess:(success)success failure:(failure)failure;

- (void)postMultipartRequestForImages:(NSArray *)images withFilenames:(NSArray *)filenames toUrl:(NSString *)urlStr withParameters:(NSDictionary *)parameters withCompletionBlockSuccess:(success)success failure:(failure)failure;


@end
