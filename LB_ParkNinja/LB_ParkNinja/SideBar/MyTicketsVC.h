//
//  MyTicketsVC.h
//  LB_ParkNinja
//
//  Created by Tric Rullan on 04/12/2015.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyTicketsVC : UIViewController

- (void)requestQRCode:(NSString *)code withCompletionBlock:(void(^)(BOOL finished, UIImage *qrCode))completion;

@end
