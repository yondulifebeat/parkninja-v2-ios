//
//  PaymentVC.m
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 12/11/15.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import "PaymentVC.h"
#import "CardInfoCell.h"
#import "AppDelegate.h"
#import "GlobalConstants.h"
#import "GlobalMethods.h"

#import "User.h"
#import "UserModel.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "Flurry.h"

@interface PaymentVC ()

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation PaymentVC
{
    User *user;
    UserModel *userModel;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupNavigationBar];
    [self setupNavigationItem];
    
    user = [GlobalMethods getUser];
    userModel = [[UserModel alloc] initWithUserModel:user];
    
    [self requestPaymentView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private Methods
- (void)setupNavigationItem
{
    [self setupRightNavigationItem:self.navigationItem];
    
    UIButton *backBtn = [UIButton buttonWithType: UIButtonTypeCustom];
    [backBtn setImage:[UIImage imageNamed:@"btn_back"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(dismissViewController) forControlEvents:UIControlEventTouchUpInside];
    [backBtn setFrame:CGRectMake(10, 0, 15, 20)];
    
    UIBarButtonItem *backBtnItem = [[UIBarButtonItem alloc]initWithCustomView:backBtn];
    
    UIBarButtonItem *leftMostItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    leftMostItem.width = -10.0f;
    
    UIBarButtonItem *fixedItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedItem.width = 10.0f;
    
    self.navigationItem.leftBarButtonItems = @[leftMostItem, fixedItem, backBtnItem];
}

- (void)setupRightNavigationItem:(UINavigationItem *)navigationItem
{
    UIButton* notifBtn = [UIButton buttonWithType: UIButtonTypeCustom];
    [notifBtn setImage:[UIImage imageNamed:@"icon_parkninja"] forState:UIControlStateNormal];
    [notifBtn setImage:[UIImage imageNamed:@"icon_parkninja"] forState:UIControlStateHighlighted];
    [notifBtn setFrame:CGRectMake(10, 0, 35, 35)];
    
    UIBarButtonItem* notif = [[UIBarButtonItem alloc]initWithCustomView: notifBtn];
    
    UIBarButtonItem *rightMostItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    rightMostItem.width = -10.0f;
    
    UIBarButtonItem *fixedItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedItem.width = 10.0f;
    
    NSArray *actionButtonItems = @[rightMostItem, fixedItem, notif];
    navigationItem.rightBarButtonItems = actionButtonItems;
}

- (void)setupNavigationBar
{
    UILabel *titleLabelView = [[UILabel alloc] initWithFrame:CGRectMake(50, 0, 200, 40)]; //<<---- Actually will be auto-resized according to frame of navigation bar;
    [titleLabelView setBackgroundColor:[UIColor clearColor]];
    [titleLabelView setTextAlignment: NSTextAlignmentCenter];
    [titleLabelView setTextColor:[UIColor whiteColor]];
    [titleLabelView setFont:[UIFont fontWithName:@"FSElliotPro-Bold" size: 20]]; //<<--- Greatest font size
    [titleLabelView setAdjustsFontSizeToFitWidth:YES]; //<<---- Allow shrink
    titleLabelView.text = @"Payment";
    self.navigationItem.titleView = titleLabelView;
}

- (void)dismissViewController
{
    if (self.isFromPaymentDetails)
    {
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    
    AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [app setRootViewControllerWithSB:SB_MAIN];
    
    [GlobalMethods saveUserStatus:YES];
}

- (void)requestPaymentView
{
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%ld", URL_ADD_CREDIT_CARD, (long)[userModel.userId integerValue]]];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    
    [self.webView loadRequest:urlRequest];
}

#pragma mark - UIWebViewDelegate
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [MBProgressHUD showHUDAddedTo:self.webView animated:YES];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [MBProgressHUD hideAllHUDsForView:self.webView animated:YES];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [Flurry logError:@"WEB ERROR" message:[error localizedDescription] error:error];
    
    [MBProgressHUD hideAllHUDsForView:self.webView animated:YES];
}

@end
