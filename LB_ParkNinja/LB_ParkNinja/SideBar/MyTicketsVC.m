//
//  MyTicketsVC.m
//  LB_ParkNinja
//
//  Created by Tric Rullan on 04/12/2015.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import "MyTicketsVC.h"
#import "TicketCell.h"
#import "GlobalConstants.h"
#import "GlobalMethods.h"
#import "HttpManagerBlocks.h"
#import "UserModel.h"
#import "User.h"
#import "TransactionModel.h"
#import "ETicketVC.h"
#import "ParkingReserveEstimationModel.h"

#import <MBProgressHUD/MBProgressHUD.h>

@interface MyTicketsVC ()

@property (weak, nonatomic) IBOutlet UITableView *ticketListTableView;

@property (strong, nonatomic) NSMutableArray *ticketListArray;
@property (strong, nonatomic) NSMutableArray *qRCodeListArray;
@end

@implementation MyTicketsVC
{
    User *user;
    UserModel *userModel;
    
    TransactionModel *transModel;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupNavigationBar];
    
    [self setupNavigationItem];
    
    [self initializeView];
    
    self.qRCodeListArray = [NSMutableArray array];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private Methods
- (void)initializeView
{
    user = [GlobalMethods getUser];
    userModel = [[UserModel alloc] initWithUserModel:user];
    
    if ([GlobalMethods isConnected])
        [self requestTransactionDetails];
}

- (void)setupNavigationItem
{
    [self setupRightNavigationItem:self.navigationItem];
    
    UIButton *backBtn = [UIButton buttonWithType: UIButtonTypeCustom];
    [backBtn setImage:[UIImage imageNamed:@"btn_back"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(dismissViewController) forControlEvents:UIControlEventTouchUpInside];
    [backBtn setFrame:CGRectMake(10, 0, 15, 20)];
    
    UIBarButtonItem *backBtnItem = [[UIBarButtonItem alloc]initWithCustomView:backBtn];
    
    UIBarButtonItem *leftMostItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    leftMostItem.width = -10.0f;
    
    UIBarButtonItem *fixedItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedItem.width = 10.0f;
    
    self.navigationItem.leftBarButtonItems = @[leftMostItem, fixedItem, backBtnItem];
}

- (void)setupRightNavigationItem:(UINavigationItem *)navigationItem
{
    UIButton* notifBtn = [UIButton buttonWithType: UIButtonTypeCustom];
    [notifBtn setImage:[UIImage imageNamed:@"icon_parkninja"] forState:UIControlStateNormal];
    [notifBtn setImage:[UIImage imageNamed:@"icon_parkninja"] forState:UIControlStateHighlighted];
    [notifBtn setFrame:CGRectMake(10, 0, 35, 35)];
    
    UIBarButtonItem* notif = [[UIBarButtonItem alloc]initWithCustomView: notifBtn];
    
    UIBarButtonItem *rightMostItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    rightMostItem.width = -10.0f;
    
    UIBarButtonItem *fixedItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedItem.width = 10.0f;
    
    NSArray *actionButtonItems = @[rightMostItem, fixedItem, notif];
    navigationItem.rightBarButtonItems = actionButtonItems;
}

- (void)setupNavigationBar
{
    UILabel *titleLabelView = [[UILabel alloc] initWithFrame:CGRectMake(50, 0, 200, 40)]; //<<---- Actually will be auto-resized according to frame of navigation bar;
    [titleLabelView setBackgroundColor:[UIColor clearColor]];
    [titleLabelView setTextAlignment: NSTextAlignmentCenter];
    [titleLabelView setTextColor:[UIColor whiteColor]];
    [titleLabelView setFont:[UIFont fontWithName:@"FSElliotPro-Bold" size: 20]]; //<<--- Greatest font size
    [titleLabelView setAdjustsFontSizeToFitWidth:YES]; //<<---- Allow shrink
    titleLabelView.text = @"Ticket List";
    self.navigationItem.titleView = titleLabelView;
}

- (void)dismissViewController
{
    AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [app setRootViewControllerWithSB:SB_MAIN];
    [GlobalMethods saveUserStatus:YES];
}

- (void)getQRCode
{
    __block NSInteger count = 0;
    
    for (int i = 0; i < [self.ticketListArray count]; i++)
    {
        TransactionModel *model = [self.ticketListArray objectAtIndex:i];
        [self requestQRCode:model.transactionCode withCompletionBlock:^(BOOL finished, UIImage *qrCode) {
            if (finished)
            {
                [self.qRCodeListArray addObject:qrCode];
                count ++;
            }
            
            if (count == [self.ticketListArray count]) {
                [self.ticketListTableView reloadData];
            }
        }];
    }
    

}

- (IBAction)saveAndCloseView:(id)sender
{
    AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [app setRootViewControllerWithSB:SB_MAIN];
    
    [GlobalMethods saveUserStatus:YES];
}

#pragma mark - API REQUEST
- (void)requestTransactionDetails
{
    MBProgressHUD *HUD = [MBProgressHUD showHUDAddedTo:[[UIApplication sharedApplication] keyWindow] animated:YES];
    HUD.dimBackground = YES;
    HUD.labelText = @"Loading";
    
    self.ticketListArray = nil;
    self.ticketListArray = [NSMutableArray array];
    
    NSDictionary *userInfo = [[NSDictionary alloc] initWithObjectsAndKeys:userModel.email, @"email", nil];
    NSLog(@"PARAMETERS: %@", userInfo);
    
    [[HttpManagerBlocks sharedInstance] postToUrl:URL_TRANSACTIONS withParameters:userInfo withCompletionBlockSuccess:^(NSDictionary *responseDictionary, RESPONSE_CODE responseCode) {
        
        for (NSArray *array in [responseDictionary objectForKey:@"results"])
        {
             TransactionModel *transactModel = [[TransactionModel alloc] initWithTransactionDictionary:(NSDictionary *)array];
            [self.ticketListArray addObject:transactModel];
        }
        
        [self.ticketListTableView reloadData]
        ;
       // [self getQRCode];
        
        [MBProgressHUD hideAllHUDsForView:[[UIApplication sharedApplication] keyWindow] animated:YES];
        
    } failure:^(NSError *error, NSString *customDescription,NSString *serverError) {
        
        UIAlertController *alert = [GlobalMethods showAlertViewWithTitle:nil message:serverError];
        [self presentViewController:alert animated:YES completion:nil];
        
        [MBProgressHUD hideAllHUDsForView:[[UIApplication sharedApplication] keyWindow] animated:YES];
    }];
}

- (void)requestQRCode:(NSString *)code withCompletionBlock:(void(^)(BOOL finished, UIImage *qrCode))completion
{
    MBProgressHUD *HUD = [MBProgressHUD showHUDAddedTo:[[UIApplication sharedApplication] keyWindow] animated:YES];
    HUD.dimBackground = YES;
    HUD.labelText = @"Loading";
    
    NSDictionary *userInfo = [[NSDictionary alloc] initWithObjectsAndKeys:code, @"transactionCode", nil];

    __block UIImage *qrCode =[UIImage imageNamed:@""];

    [[HttpManagerBlocks sharedInstance] postQRCodeToUrl:URL_GET_QR_CODE withParameters:userInfo withCompletionBlockSuccess:^(UIImage *response) {
        
        qrCode = response;
        completion(YES, qrCode);
        
        [MBProgressHUD hideAllHUDsForView:[[UIApplication sharedApplication] keyWindow] animated:YES];
        
    } failure:^(NSError *error, NSString *customDescription, NSString *serverError) {
        completion(NO, qrCode);
        
        [MBProgressHUD hideAllHUDsForView:[[UIApplication sharedApplication] keyWindow] animated:YES];
    }];
}

#pragma mark - UITableViewCell
- (TicketCell *)setTicketCellWithRow:(NSInteger)row
{
    TicketCell *cell = [self.ticketListTableView dequeueReusableCellWithIdentifier:@"TicketCell"];
    
    if (cell == nil)
    {
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"TicketCell" owner:self options:nil];
        cell = (TicketCell *)nibArray[0];
    }
    
    [cell initButtonView];
    
    TransactionModel *transactModel = [self.ticketListArray objectAtIndex:row];
    cell.referenceLabel.text = transactModel.transactionCode;
    cell.addressLabel.text = transactModel.parkingLocation;

    
    return cell;
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.ticketListArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self setTicketCellWithRow:indexPath.row];
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    transModel = [self.ticketListArray objectAtIndex:indexPath.row];
    
    [self performSegueWithIdentifier:SEGUE_SHOW_ETICKET_FROM_LIST sender:self];
    
    [self.ticketListTableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    ETicketVC *vc = (ETicketVC *)segue.destinationViewController;
    
    vc.parkingLotModel = [[ParkingLotModel alloc] initWithTransactionModel:transModel];
    vc.estimateModel = [[ParkingReserveEstimationModel alloc] initParkingEstimationWithTransactionModel:transModel];
 
    NSLog(@"ESTIMATE: %ld", (long)vc.estimateModel.estimateAmount);
    
    vc.qrCode = transModel.transactionCode;
}

@end
