//
//  CardInfoCell.m
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 12/11/15.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import "CardInfoCell.h"

@implementation CardInfoCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
