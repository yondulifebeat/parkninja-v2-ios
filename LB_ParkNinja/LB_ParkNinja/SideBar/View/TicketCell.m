//
//  TicketCell.m
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 12/29/15.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import "TicketCell.h"

@implementation TicketCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)initButtonView
{
    self.navigateBtn.layer.cornerRadius = 5;
    self.navigateBtn.clipsToBounds = YES;
    
    self.cancelBtn.layer.cornerRadius = 5;
    self.cancelBtn.clipsToBounds = YES;
}

@end
