//
//  VehicleCell.h
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 12/15/15.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VehicleCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *carDetailsLabel;
@property (weak, nonatomic) IBOutlet UIButton *editButton;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;
@property (weak, nonatomic) IBOutlet UIView *borderView;

@end
