//
//  HomeNC.h
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 11/24/15.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeNC : UINavigationController

+ (HomeNC *)sharedInstance;

- (void)setHomeHoverVC;

@end
