//
//  ParkListingVC.m
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 11/23/15.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import "ParkListingVC.h"
#import "AppDelegate.h"

#import "ParkingInfoCell.h"
#import "HomeVC.h"
#import "HomeNC.h"
#import "ParkingLotDetailsVC.h"
#import "GlobalConstants.h"
#import "ParkingLotModel.h"
#import "GlobalMethods.h"
#import "HttpManagerBlocks.h"
#import <MBProgressHUD/MBProgressHUD.h>

@interface ParkListingVC ()

@property (weak, nonatomic) IBOutlet UITableView *parkingListTableView;

@property (strong, nonatomic) NSMutableArray *parkingList;

@property (strong, nonatomic) UISearchController *searchController;

@property (strong, nonatomic) NSMutableArray *searchResultsMArray;

@end

@implementation ParkListingVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.parkingList = [NSMutableArray array];
    
    if ([GlobalMethods isConnected])
        [self requestAllParkingList];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(dismissViewCiontroller:)
                                                 name:@"DismissList"
                                               object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES];
}

#pragma mark - API Request
- (void)requestAllParkingList
{
    self.parkingList = [NSMutableArray array];
    
    MBProgressHUD *HUD = [MBProgressHUD showHUDAddedTo:[[UIApplication sharedApplication] keyWindow] animated:YES];
    HUD.dimBackground = YES;
    HUD.labelText = @"Loading";
    
    [[HttpManagerBlocks sharedInstance] getFromUrl:URL_PARK_LIST withParameters:nil withCompletionBlockSuccess:^(NSDictionary *responseDictionary, RESPONSE_CODE responseCode){
        
        for (NSArray *array in [responseDictionary objectForKey:@"results"])
        {
            ParkingLotModel *parkingModel = [[ParkingLotModel alloc] initWithParkingLotDictionary:(NSDictionary *)array];
            [self.parkingList addObject:parkingModel];
        }
        
        [self.parkingListTableView reloadData];
        
        [MBProgressHUD hideAllHUDsForView:[[UIApplication sharedApplication] keyWindow] animated:YES];

    } failure:^(NSError *error, NSString *customDescription, NSString *serverError) {
        
        //UIAlertController *alert = [GlobalMethods showAlertViewWithTitle:nil message:serverError];
        //[self presentViewController:alert animated:YES completion:nil];
        
        [MBProgressHUD hideAllHUDsForView:[[UIApplication sharedApplication] keyWindow] animated:YES];
        NSLog(@"SERVER ERROR: %@", serverError);
    }];
}

#pragma mark - ParkingInfoCell
- (ParkingInfoCell *)setParkingDetailsWithRow:(NSInteger)row
{
    ParkingInfoCell *cell = [self.parkingListTableView dequeueReusableCellWithIdentifier:@"ParkingInfoCell"];
    
    if (cell == nil)
    {
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"ParkingInfoCell" owner:self options:nil];
        cell = (ParkingInfoCell *)nibArray[0];
    }
    
    ParkingLotModel *model = [self.parkingList objectAtIndex:row];
    [cell.mainAddressLabel setText:model.location];
    [cell.detailsAddressLabel setText:model.location];
    
    return cell;
}

#pragma mark - UIButtonAction
- (IBAction)dismissViewCiontroller:(id)sender
{
    [self.navigationController.navigationBar setHidden:YES];
    [[HomeVC sharedInstance] hideContentController:self];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.parkingList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self setParkingDetailsWithRow:indexPath.row];
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[HomeVC sharedInstance] hideContentController:self];
    
    ParkingLotModel *model = [self.parkingList objectAtIndex:indexPath.row];
    
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:model forKey:@"ParkingModel"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"RequestRoute" object:nil userInfo:userInfo];
    
    [self.parkingListTableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
