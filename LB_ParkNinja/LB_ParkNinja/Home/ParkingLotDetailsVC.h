//
//  ParkingLotDetailsVC.h
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 11/26/15.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParkingLotModel.h"

@protocol ParkingLotDetailsDelegate;

@interface ParkingLotDetailsVC : UIViewController

@property (strong, nonatomic) ParkingLotModel *model;
@property (strong, nonatomic) id <ParkingLotDetailsDelegate> delegate;

@property (strong, nonatomic) NSString *distance;
@property (strong, nonatomic) NSString *locationAddress;

@end

@protocol ParkingLotDetailsDelegate <NSObject>

- (void)requestNavigationWithModel:(ParkingLotModel *)parkingLot;

@end
