//
//  SearchResultsTVC.h
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 1/10/16.
//  Copyright © 2016 Tricia Rullan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeVC.h"

@interface SearchResultsTVC : UIViewController <UISearchBarDelegate, UISearchResultsUpdating, UISearchDisplayDelegate>

@property (strong, nonatomic) NSMutableArray *searchResultsMArray;
@property (strong, nonatomic) NSMutableArray *parkingListArray;

@property (assign, nonatomic) BOOL hideSearchBar;
@end
