//
//  HomeSB.h
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 11/16/15.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import <MapKit/MapKit.h>

#import "LocationManager.h"

@protocol HomeVCDelegate <NSObject>
- (void)updateSearchWithText:(NSString *)search;
@end

@interface HomeVC : UIViewController <GMSMapViewDelegate, LocationManagerDelegate>

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (strong, nonatomic) UINavigationController *navController;
@property (assign, nonatomic) id <HomeVCDelegate> delegate;

+ (HomeVC *)sharedInstance;

- (void)hideContentController:(UIViewController *)content;
- (void)requestAllParkingList:(void (^) (BOOL finished))completion;


@end
