//
//  ETicketVC.m
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 1/4/16.
//  Copyright © 2016 Tricia Rullan. All rights reserved.
//

#import "ETicketVC.h"
#import "ETicketCell.h"
#import "AppDelegate.h"
#import "GlobalConstants.h"
#import "GlobalMethods.h"

#import <MBProgressHUD/MBProgressHUD.h>
#import "HttpManagerBlocks.h"
#import "GlobalMethods.h"
#import "User.h"
#import "UserModel.h"

@interface ETicketVC ()

@property (weak, nonatomic) IBOutlet UITableView *eTicketTableView;
@property (strong, nonatomic) UIImage *qrCodeImage;

@end

@implementation ETicketVC
{
    User *user;
    UserModel *userModel;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupNavigationBar];
    [self setupNavigationItem];
    
    user = [GlobalMethods getUser];
    userModel = [[UserModel alloc] initWithUserModel:user];
    
    [self requestQRCode];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Private Methods
- (void)setupNavigationItem
{
    [self setupRightNavigationItem:self.navigationItem];
    
    UIButton *backBtn = [UIButton buttonWithType: UIButtonTypeCustom];
    [backBtn setImage:[UIImage imageNamed:@"btn_back"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(dismissViewController) forControlEvents:UIControlEventTouchUpInside];
    [backBtn setFrame:CGRectMake(10, 0, 15, 20)];
    
    UIBarButtonItem *backBtnItem = [[UIBarButtonItem alloc]initWithCustomView:backBtn];
    
    UIBarButtonItem *leftMostItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    leftMostItem.width = -10.0f;
    
    UIBarButtonItem *fixedItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedItem.width = 10.0f;
    
    self.navigationItem.leftBarButtonItems = @[leftMostItem, fixedItem, backBtnItem];
}

- (void)setupRightNavigationItem:(UINavigationItem *)navigationItem
{
    UIButton* notifBtn = [UIButton buttonWithType: UIButtonTypeCustom];
    [notifBtn setImage:[UIImage imageNamed:@"icon_parkninja"] forState:UIControlStateNormal];
    [notifBtn setImage:[UIImage imageNamed:@"icon_parkninja"] forState:UIControlStateHighlighted];
    [notifBtn setFrame:CGRectMake(10, 0, 35, 35)];
    
    UIBarButtonItem* notif = [[UIBarButtonItem alloc]initWithCustomView: notifBtn];
    
    UIBarButtonItem *rightMostItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    rightMostItem.width = -10.0f;
    
    UIBarButtonItem *fixedItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedItem.width = 10.0f;
    
    NSArray *actionButtonItems = @[rightMostItem, fixedItem, notif];
    navigationItem.rightBarButtonItems = actionButtonItems;
}

- (void)setupNavigationBar
{
    UILabel *titleLabelView = [[UILabel alloc] initWithFrame:CGRectMake(50, 0, 200, 40)]; //<<---- Actually will be auto-resized according to frame of navigation bar;
    [titleLabelView setBackgroundColor:[UIColor clearColor]];
    [titleLabelView setTextAlignment: NSTextAlignmentCenter];
    [titleLabelView setTextColor:[UIColor whiteColor]];
    [titleLabelView setFont:[UIFont fontWithName:@"FSElliotPro-Bold" size: 20]]; //<<--- Greatest font size
    [titleLabelView setAdjustsFontSizeToFitWidth:YES]; //<<---- Allow shrink
    titleLabelView.text = @"Congratulations";
    self.navigationItem.titleView = titleLabelView;
}

- (void)dismissViewController
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - IBAction
- (IBAction)saveAndCloseTicket:(id)sender
{
    AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [app setRootViewControllerWithSB:SB_MAIN];
    
    [GlobalMethods saveUserStatus:YES];
}

#pragma mark - API Request
- (void)requestQRCode
{
    MBProgressHUD *HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.dimBackground = YES;
    HUD.labelText = @"Loading";
    
    NSMutableDictionary *userInfo = [[NSMutableDictionary alloc] initWithObjectsAndKeys: self.qrCode, @"transactionCode", nil];
    
    NSLog(@"USER INFO: %@", userInfo);
    
    [[HttpManagerBlocks sharedInstance] postQRCodeToUrl:URL_GET_QR_CODE withParameters:userInfo withCompletionBlockSuccess:^(UIImage *response) {
        
        self.qrCodeImage = response;
        
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
        [self.eTicketTableView reloadData];
        
    } failure:^(NSError *error, NSString *customDescription, NSString *serverError) {
        
        UIAlertController *alert =  [GlobalMethods showAlertViewWithTitle:nil message:serverError];
        [self presentViewController:alert animated:YES completion:nil];
        
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
    }];
}

#pragma mark - ParkingInfoCell
- (ETicketCell *)setETicketCellWithRow:(NSInteger)row
{
    ETicketCell *cell = [self.eTicketTableView dequeueReusableCellWithIdentifier:@"ETicketCell"];
    
    if (cell == nil)
    {
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"ETicketCell" owner:self options:nil];
        cell = (ETicketCell *)nibArray[0];
    }
    
    cell.eTicketContainerView.layer.cornerRadius = 15.0f;
    cell.eTicketContainerView.layer.masksToBounds = YES;
    
    
    cell.nameLabel.text = [NSString stringWithFormat:@"%@ %@", userModel.firstName, userModel.lastName];
    cell.qrCodeImage.image = self.qrCodeImage;
    cell.parkSiteLabel.text = self.parkingLotModel.location;
    cell.mobileNumLabel.text = user.mobileNo;
    
    
    if (![GlobalMethods checkInputForWhiteSpaces:self.carModel.plateNum])
         cell.plateNumLabel.text = @"Not Available";
    
    else
        cell.plateNumLabel.text = self.carModel.plateNum;
    
    
    NSString *carMake = self.carModel.make;
    NSString *carModel = self.carModel.make;
    NSString *carColor = self.carModel.make;
    
    if (![GlobalMethods checkInputForWhiteSpaces:self.carModel.make])
        carMake = self.carModel.make = @"Not Available";
    
    if (![GlobalMethods checkInputForWhiteSpaces:self.carModel.model])
        carModel = self.carModel.model = @"Not Available";
   
    if (![GlobalMethods checkInputForWhiteSpaces:self.carModel.color])
        carColor = self.carModel.color = @"Not Available";
    
    cell.carDetailsLabel.text = [NSString stringWithFormat:@"Make:%@ \nModel:%@ \nColor:%@", carMake, carModel, carColor];
    
    
    cell.totalPriceLabel.text = [NSString stringWithFormat:@"Php %ld.00 (Payment computation begins upon finalizing the reservation.)",(long)self.estimateModel.estimateAmount];
    cell.referenceNumberLabel.text = self.qrCode;
    
    NSDate *dateReserveFrom = [NSDate dateWithTimeIntervalSince1970:(self.estimateModel.reserveFrom / 1000)];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setTimeZone:[NSTimeZone timeZoneWithName:@"PHT"]];
    [dateFormat setDateFormat:@"MM/dd/yyyy hh:mm a"];
    NSString *dateReserveFromString = [dateFormat stringFromDate:dateReserveFrom];
    
    NSDate *dateReserveTo = [NSDate dateWithTimeIntervalSince1970:(self.estimateModel.reserveTo / 1000)];
    
    NSString *dateReserveToString = [dateFormat stringFromDate:dateReserveTo];
    
    cell.dateReservationLabel.text = [NSString stringWithFormat:@"%@ - %@", dateReserveFromString, dateReserveToString];
    
    return cell;
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self setETicketCellWithRow:indexPath.row];
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 650.0f;
}

@end
