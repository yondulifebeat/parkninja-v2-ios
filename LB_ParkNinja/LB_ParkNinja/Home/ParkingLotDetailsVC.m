//
//  ParkingLotDetailsVC.m
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 11/26/15.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import "ParkingLotDetailsVC.h"
#import "ParkingLotDetailsCell.h"
#import "GlobalMethods.h"
#import "UserInformationVC.h"

#import "GlobalConstants.h"
#import "HttpManagerBlocks.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "HomeVC.h"

@interface ParkingLotDetailsVC ()

@property (strong, nonatomic) IBOutlet UITableView *parkingLotDetailsTableView;
@property (strong, nonatomic) NSMutableArray *parkingList;

@end

@implementation ParkingLotDetailsVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationController setNavigationBarHidden:NO];

    [self requestUpdateProfileDetails];
    
    [self setupNavigationBar];
    [self setupNavigationItem:self.navigationItem];
    
    [self.navigationController.navigationBar setHidden:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private Methods
- (void)setupNavigationItem:(UINavigationItem*)navigationItem
{
    [self setupRightNavigationItem:navigationItem];
    
    UIButton *backBtn = [UIButton buttonWithType: UIButtonTypeCustom];
    [backBtn setImage:[UIImage imageNamed:@"btn_back"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(dismissViewController) forControlEvents:UIControlEventTouchUpInside];
    [backBtn setFrame:CGRectMake(10, 0, 15, 20)];
    
    UIBarButtonItem *backBtnItem = [[UIBarButtonItem alloc]initWithCustomView:backBtn];
    
    UIBarButtonItem *leftMostItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    leftMostItem.width = -10.0f;
    
    UIBarButtonItem *fixedItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedItem.width = 10.0f;
    
    navigationItem.leftBarButtonItems = @[leftMostItem, fixedItem, backBtnItem];
}

- (void)setupRightNavigationItem:(UINavigationItem *)navigationItem
{
    UIButton* notifBtn = [UIButton buttonWithType: UIButtonTypeCustom];
    [notifBtn setImage:[UIImage imageNamed:@"icon_parkninja"] forState:UIControlStateNormal];
    [notifBtn setImage:[UIImage imageNamed:@"icon_parkninja"] forState:UIControlStateHighlighted];
    [notifBtn setFrame:CGRectMake(10, 0, 35, 35)];
    
    UIBarButtonItem* notif = [[UIBarButtonItem alloc]initWithCustomView: notifBtn];
    
    UIBarButtonItem *rightMostItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    rightMostItem.width = -10.0f;
    
    UIBarButtonItem *fixedItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedItem.width = 10.0f;
    
    NSArray *actionButtonItems = @[rightMostItem, fixedItem, notif];
    navigationItem.rightBarButtonItems = actionButtonItems;
}

- (void)setupNavigationBar
{
    UILabel *titleLabelView = [[UILabel alloc] initWithFrame:CGRectMake(50, 0, 200, 40)]; //<<---- Actually will be auto-resized according to frame of navigation bar;
    [titleLabelView setBackgroundColor:[UIColor clearColor]];
    [titleLabelView setTextAlignment: NSTextAlignmentCenter];
    [titleLabelView setTextColor:[UIColor whiteColor]];
    [titleLabelView setFont:[UIFont fontWithName:@"FSElliotPro-Bold" size: 20]]; //<<--- Greatest font size
    [titleLabelView setAdjustsFontSizeToFitWidth:YES]; //<<---- Allow shrink
    titleLabelView.text = _model.location;
    self.navigationItem.titleView = titleLabelView;
}

- (void)dismissViewController
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - IBAction
- (IBAction)reserveNow:(id)sender
{
    [self performSegueWithIdentifier:SEGUE_SHOW_USER_INFORMATION sender:self];
}

#pragma mark - UIViewDelegate
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    UserInformationVC *vc = (UserInformationVC *)segue.destinationViewController;
    vc.parkingLotModel = self.model;
    vc.locationAddress = self.locationAddress;
}

#pragma mark - API Request
- (void)requestUpdateProfileDetails
{
    MBProgressHUD *HUD = [MBProgressHUD showHUDAddedTo:[[UIApplication sharedApplication] keyWindow]animated:YES];
    HUD.dimBackground = YES;
    HUD.labelText = @"Loading";
    
    NSDictionary *userInfo = [[NSDictionary alloc] initWithObjectsAndKeys: [NSNumber numberWithInteger:_model.parkingId], @"parkingLotId", nil];
    
    NSLog(@"PARAMETERS: %@", userInfo);
    
    [[HttpManagerBlocks sharedInstance] postToUrl:URL_PARKING_DETAILS withParameters:userInfo withCompletionBlockSuccess:^(NSDictionary *responseDictionary, RESPONSE_CODE responseCode) {
        
        _model = [[ParkingLotModel alloc] initWithParkingLotDictionary:responseDictionary];
        
        [self.parkingLotDetailsTableView reloadData];
        
        [MBProgressHUD hideAllHUDsForView:[[UIApplication sharedApplication] keyWindow] animated:YES];

    } failure:^(NSError *error, NSString *customDescription, NSString *serverError) {

        [MBProgressHUD hideAllHUDsForView:[[UIApplication sharedApplication] keyWindow] animated:YES];
    }];
}

#pragma mark - ParkingInfoCell
- (ParkingLotDetailsCell *)setParkingDetailsWithRow:(NSInteger)row
{
    ParkingLotDetailsCell *cell = [self.parkingLotDetailsTableView dequeueReusableCellWithIdentifier:@"ParkingLotDetailsCell"];
    
    if (cell == nil)
    {
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"ParkingLotDetailsCell" owner:self options:nil];
        cell = (ParkingLotDetailsCell *)nibArray[0];
    }
    
    cell.parkingNameLabel.text = self.model.location;
    cell.parkingAddressLabel.text = self.locationAddress;
    cell.operatingHoursLabel.text = self.model.operatingHours;
    cell.hourlyRateLabel.text = self.self.model.hourlyRate;
    cell.typeLabel.text = self.model.type;
    cell.distanceLabel.text = self.distance;
    
    [cell.navigateMeBtn addTarget:self action:@selector(navigateParkingLocation:) forControlEvents:UIControlEventTouchUpInside];
    cell.navigateMeBtn.tag = row;
    
    return cell;
}

- (void)navigateParkingLocation:(id)sender
{
    [GlobalMethods saveUserStatus:YES];
    
    [self dismissViewController];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self setParkingDetailsWithRow:indexPath.row];
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 310.0f;
}

@end
