//
//  ResultsVC.h
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 1/10/16.
//  Copyright © 2016 Tricia Rullan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResultsVC : UIViewController

@property (strong, nonatomic) NSMutableArray *searchResultsMArray;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
