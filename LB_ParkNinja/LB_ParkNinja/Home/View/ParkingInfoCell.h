//
//  ParkingInfoCell.h
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 11/23/15.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ParkingInfoCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *mainAddressLabel;
@property (strong, nonatomic) IBOutlet UILabel *detailsAddressLabel;

@end
