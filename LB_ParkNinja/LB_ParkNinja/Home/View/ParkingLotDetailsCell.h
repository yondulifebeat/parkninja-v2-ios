//
//  ParkingLotDetailsCell.h
//  LB_ParkNinja
//
//  Created by Tric Rullan on 01/12/2015.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ParkingLotDetailsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *parkingNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *parkingAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *availableSlotsLabel;

@property (weak, nonatomic) IBOutlet UILabel *operatingHoursLabel;
@property (weak, nonatomic) IBOutlet UILabel *hourlyRateLabel;
@property (weak, nonatomic) IBOutlet UILabel *typeLabel;

@property (weak, nonatomic) IBOutlet UILabel *contactNumLabel;

@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;

@property (weak, nonatomic) IBOutlet UIButton *navigateMeBtn;

@end
