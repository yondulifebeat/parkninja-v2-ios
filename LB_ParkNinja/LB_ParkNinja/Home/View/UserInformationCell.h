//
//  UserInformationCell.h
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 12/2/15.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserInformationCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UITextField *mobileNumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *carMakeTextField;
@property (weak, nonatomic) IBOutlet UITextField *carModelTextField;
@property (weak, nonatomic) IBOutlet UITextField *carColorTextField;
@property (weak, nonatomic) IBOutlet UITextField *plateNumTextField;

@property (weak, nonatomic) IBOutlet UIButton *reserveFromDateButton;

@property (weak, nonatomic) IBOutlet UIButton *reserveToDateButton;
@property (weak, nonatomic) IBOutlet UIButton *reserveTimeButton;

@property (weak, nonatomic) IBOutlet UITextField *promoCodeTextField;



@end
