//
//  ETicketCell.h
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 1/5/16.
//  Copyright © 2016 Tricia Rullan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ETicketCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *eTicketContainerView;
@property (weak, nonatomic) IBOutlet UILabel *referenceNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *parkSiteLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@property (weak, nonatomic) IBOutlet UILabel *mobileNumLabel;
@property (weak, nonatomic) IBOutlet UILabel *plateNumLabel;

@property (weak, nonatomic) IBOutlet UILabel *totalPriceLabel;
@property (weak, nonatomic) IBOutlet UIImageView *qrCodeImage;
@property (weak, nonatomic) IBOutlet UILabel *carDetailsLabel;

@property (weak, nonatomic) IBOutlet UILabel *dateReservationLabel;
@end
