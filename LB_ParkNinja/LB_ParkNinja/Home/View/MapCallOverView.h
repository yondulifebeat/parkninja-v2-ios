//
//  MapCallOverView.h
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 11/26/15.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MapCallOverView : UIView

@property (weak, nonatomic) IBOutlet UILabel *addressLabel;

@end
