//
//  ParkingSummaryCell.h
//  LB_ParkNinja
//
//  Created by Tric Rullan on 03/12/2015.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ParkingSummaryCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UILabel *parkingNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *mobileNumLabel;
@property (weak, nonatomic) IBOutlet UILabel *carMakeLabel;
@property (weak, nonatomic) IBOutlet UILabel *carModelLabel;
@property (weak, nonatomic) IBOutlet UILabel *carColorLabel;
@property (weak, nonatomic) IBOutlet UILabel *plateNumLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateReservationLabel;
@property (weak, nonatomic) IBOutlet UILabel *estimatePriceLabel;

@end
