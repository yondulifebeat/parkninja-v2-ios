//
//  SideBarVC.m
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 11/23/15.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import "SideBarVC.h"

#import "ProfileVC.h"
#import "MyTicketsVC.h"
#import "TermsAndConditionsVC.h"
#import "FAQVC.h"
#import "AboutVC.h"
#import "HomeVC.h"
#import "ProfileImageCell.h"
#import "User.h"
#import "UserModel.h"
#import "GlobalMethods.h"

typedef NS_ENUM (NSInteger, SIDE_CELL_INDEX)
{
    SIDE_PROFILE = 0,
    SIDE_HOME,
    SIDE_MY_TIX,
    SIDE_TERMS,
    SIDE_FAQ,
    SIDE_FEEDBACK,
    SIDE_SETTINGS,
};

@interface SideBarVC ()

@property (strong, nonatomic) IBOutlet UITableView *sideBarTableView;

@end

@implementation SideBarVC
{
    User *user;
    UserModel *userModel;
    
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    user = [GlobalMethods getUser];
    userModel = [[UserModel alloc] initWithUserModel:user];
    [userModel initWithUserProfileImageData:user];
}

#pragma mark - Private Methods
- (void)setupNavigationItem
{
    [self setupRightNavigationItem:self.navigationItem];
    
    UIButton *backBtn = [UIButton buttonWithType: UIButtonTypeCustom];
    [backBtn setImage:[UIImage imageNamed:@"btn_back"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(dismissViewController) forControlEvents:UIControlEventTouchUpInside];
    [backBtn setFrame:CGRectMake(10, 0, 15, 20)];
    
    UIBarButtonItem *backBtnItem = [[UIBarButtonItem alloc]initWithCustomView:backBtn];
    
    UIBarButtonItem *leftMostItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    leftMostItem.width = -10.0f;
    
    UIBarButtonItem *fixedItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedItem.width = 10.0f;
    
    self.navigationItem.leftBarButtonItems = @[leftMostItem, fixedItem, backBtnItem];
}

- (void)setupRightNavigationItem:(UINavigationItem *)navigationItem
{
    UIButton* notifBtn = [UIButton buttonWithType: UIButtonTypeCustom];
    [notifBtn setImage:[UIImage imageNamed:@"icon_parkninja"] forState:UIControlStateNormal];
    [notifBtn setImage:[UIImage imageNamed:@"icon_parkninja"] forState:UIControlStateHighlighted];
    [notifBtn setFrame:CGRectMake(10, 0, 35, 35)];
    
    UIBarButtonItem* notif = [[UIBarButtonItem alloc]initWithCustomView: notifBtn];
    
    UIBarButtonItem *rightMostItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    rightMostItem.width = -10.0f;
    
    UIBarButtonItem *fixedItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedItem.width = 10.0f;
    
    NSArray *actionButtonItems = @[rightMostItem, fixedItem, notif];
    navigationItem.rightBarButtonItems = actionButtonItems;
}

- (void)setupNavigationBar
{
    UILabel *titleLabelView = [[UILabel alloc] initWithFrame:CGRectMake(50, 0, 200, 40)]; //<<---- Actually will be auto-resized according to frame of navigation bar;
    [titleLabelView setBackgroundColor:[UIColor clearColor]];
    [titleLabelView setTextAlignment: NSTextAlignmentCenter];
    [titleLabelView setTextColor:[UIColor whiteColor]];
    //[titleLabelView setFont:[UIFont fontWithName:@"Gotham Medium" size: 23]]; //<<--- Greatest font size
    [titleLabelView setAdjustsFontSizeToFitWidth:YES]; //<<---- Allow shrink
    titleLabelView.text = @"6750 Steel Car Park";
    self.navigationItem.titleView = titleLabelView;
}

- (void)dismissViewController
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)showUserInformation
{
    //    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"HomeSB" bundle:nil];
    //    UserInformationVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"UserInformationVC"];
    //    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - UITableViewCell
- (ProfileImageCell *)setProfileImageCellWithRow:(NSInteger)row
{
    ProfileImageCell *cell = [self.sideBarTableView dequeueReusableCellWithIdentifier:@"display"];
    
    if (cell == nil)
    {
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"ProfileImageCell" owner:self options:nil];
        cell = (ProfileImageCell *)nibArray[0];
    }
    
    cell.nameLabel.text = [NSString stringWithFormat:@"%@ %@", user.firstName, user.lastName];
    cell.profileImageView.image = [UIImage imageWithData:user.displayImage];
    
    return cell;
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 7;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"profile";
    
    switch (indexPath.row)
    {
        case SIDE_PROFILE:
        {
            return [self setProfileImageCellWithRow:indexPath.row];
        }
            
        case SIDE_HOME:
        {
            cellIdentifier = @"home";
            break;
        }

        case SIDE_MY_TIX:
        {
            cellIdentifier = @"tickets";
            break;
        }
        case SIDE_TERMS:
        {
            cellIdentifier = @"payment";
            break;
        }
        case SIDE_FAQ:
        {
            cellIdentifier = @"vehicles";
            break;
        }
        case SIDE_FEEDBACK:
        {
            cellIdentifier = @"profile";
            break;
        }
            
        case SIDE_SETTINGS:
        {
            cellIdentifier = @"about";
            break;
        }
    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
        return 250.0f;
    
    return 50.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.sideBarTableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
