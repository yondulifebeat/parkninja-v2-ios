//
//  SearchResultsTVC.m
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 1/10/16.
//  Copyright © 2016 Tricia Rullan. All rights reserved.
//

#import "SearchResultsTVC.h"
#import "AppDelegate.h"
#import "ParkingLotModel.h"
#import "ResultsVC.h"

@interface SearchResultsTVC ()

@property (nonatomic, strong) UISearchController *searchController;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation SearchResultsTVC

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.navigationController.navigationBar.hidden = NO;
    self.navigationItem.backBarButtonItem.title = @"";
    
    //AppDelegate *app = [[UIApplication sharedApplication] delegate];
    //[app changeNavBarToClearColor];
    
    UINavigationController *searchResultsNavigationController = [[self storyboard] instantiateViewControllerWithIdentifier:@"searchResultsSid"];
    
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:searchResultsNavigationController];
    self.searchController.searchResultsUpdater = self;
    self.searchController.searchBar.frame = CGRectMake(self.searchController.searchBar.frame.origin.x,
                                                       self.searchController.searchBar.frame.origin.y,
                                                       self.searchController.searchBar.frame.size.width, 44.0);
    
    [self.searchController.searchBar setDelegate:self];

    self.tableView.tableHeaderView = self.searchController.searchBar;
    
    [self.searchController.searchBar becomeFirstResponder];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(dismissViewController)
                                                 name:@"DismissSearch"
                                               object:nil];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dismissViewController
{
    self.navigationController.navigationBar.hidden = YES;
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table view data source
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 25.0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.searchResultsMArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    ParkingLotModel *model =  [self.searchResultsMArray objectAtIndex:indexPath.row];
    cell.textLabel.text = model.location;
    
    cell.accessoryType =  UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ParkingLotModel *model = [self.searchResultsMArray objectAtIndex:indexPath.row];
    
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:model forKey:@"ParkingModel"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"RequestRoute" object:nil userInfo:userInfo];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DismissList" object:nil];
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 1.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *customView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 1)];
    return customView;
}

#pragma mark - SearchControllerDelegate -- iOS 8 and above
- (void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    // Set searchString equal to what's typed into the searchbar
    NSString *searchString = self.searchController.searchBar.text;
    
    [self updateFilteredContentForContactName:searchString];

    if (self.searchController.searchResultsController) {
        
        UINavigationController *navController = (UINavigationController *)self.searchController.searchResultsController;
        
        // Present SearchResultsTableViewController as the topViewController
        ResultsVC *searchTVC = (ResultsVC *)navController.topViewController;
        
        // Update searchResults
        searchTVC.searchResultsMArray = self.searchResultsMArray;
        
        // And reload the tableView with the new data
        [searchTVC.tableView reloadData];
    }

}

- (void)updateFilteredContentForContactName:(NSString *)parkingLoc
{
    if (parkingLoc == nil)
    {
        self.searchResultsMArray = [self.parkingListArray mutableCopy];
    } else
    {
        NSString *dictionaryKey = @"location"; //NSDictionary key we wish to search
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K CONTAINS[cd] %@", dictionaryKey, parkingLoc]; //create our predicate
        
        NSMutableArray *searchResults = [[NSMutableArray alloc] initWithArray:[self.parkingListArray filteredArrayUsingPredicate:predicate]]; //get new array containing objects matching predicate
        self.searchResultsMArray = searchResults;
    }
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    AppDelegate *app = [[UIApplication sharedApplication] delegate];
    [app changeNavBarToOriginal];
    
    [self.navigationController popViewControllerAnimated:YES];
    
    self.navigationController.navigationBar.hidden = YES;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DismissList" object:nil];
}

@end
