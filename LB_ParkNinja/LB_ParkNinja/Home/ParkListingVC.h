//
//  ParkListingVC.h
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 11/23/15.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParkingLotModel.h"

@interface ParkListingVC : UIViewController

@property (strong, nonatomic) ParkingLotModel *model;

@end
