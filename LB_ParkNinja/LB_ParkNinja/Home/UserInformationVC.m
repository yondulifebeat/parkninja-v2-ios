//
//  UserInformationVC.m
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 12/2/15.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import "UserInformationVC.h"
#import "UserInformationCell.h"
#import "ParkingLotModel.h"
#import "GlobalMethods.h"
#import "HttpManagerBlocks.h"
#import "GlobalConstants.h"
#import "ParkingReserveEstimationModel.h"
#import "UserModel.h"
#import "CarModel.h"
#import "User.h"
#import "UserModel.h"
#import "ParkingSummaryVC.h"
#import "GlobalMethods.h"
#import "PromoCodeModel.h"

#import <MBProgressHUD/MBProgressHUD.h>

typedef NS_ENUM(NSInteger, USER_TEXTFIELD_INDEX)
{
    TF_INDEX_MOBILE,
    TF_INDEX_CAR_MAKE,
    TF_INDEX_CAR_MODEL,
    TF_INDEX_COLOR,
    TF_INDEX_PLATE_NUM,
    TF_INDEX_PROMO_CODE
};

typedef NS_ENUM(NSInteger, PICKER_INDEX)
{
    PICKER_DATE_FROM,
    PICKER_TIME_FROM,
    PICKER_DATE_TO,
    PICKER_TIME_TO
};

@interface UserInformationVC ()

@property (weak, nonatomic) IBOutlet UITableView *userInfoTableView;
@property (strong, nonatomic) NSMutableArray *parkingList;

@property (strong, nonatomic) ParkingReserveEstimationModel *estimateModel;
@property (strong, nonatomic) PromoCodeModel *promoCodeModel;

@property (weak, nonatomic) IBOutlet UIDatePicker *datePickerView;
@property (weak, nonatomic) IBOutlet UIView *pickerContainerView;

@property (assign, nonatomic) NSInteger buttonTag;
@property (strong, nonatomic) NSMutableArray *carListArray;
@property (strong, nonatomic) NSMutableArray *promoListArray;

@end

@implementation UserInformationVC
{
    UserModel *userModel;
    User *user;
    CarModel *car;
    
    NSString *reservationDateFrom;
    NSString *reservationDateTo;
    NSString *reservationTimeTo;
    
    long serverTime;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupNavigationBar];
    [self setupNavigationItem];
    
    if (self.estimateModel == nil)
        self.estimateModel = [[ParkingReserveEstimationModel alloc] init];
    
    user = [GlobalMethods getUser];
    userModel = [[UserModel alloc] initWithUserModel:user];
    
    car = [[CarModel alloc] init];
    
    //if ([GlobalMethods isConnected])
    //{
        dispatch_async(dispatch_get_main_queue(), ^{
            [self requestCarDetails];
            [self requestCurrentServerTime];
            [self requestPromoList];
        });
    //}
  
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private Methods
- (void)setCurrentDateTimeWithMS:(long)timestamp
{
    serverTime = timestamp;
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:(timestamp / 1000)];

    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setTimeZone:[NSTimeZone timeZoneWithName:@"PHT"]];
    [dateFormat setDateFormat:@"MM/dd/yyyy hh:mm a"];
    NSString *dateString = [dateFormat stringFromDate:date];
    NSLog(@"date: %@", dateString);
    
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    NSString *dateOnly = [dateFormat stringFromDate:date];

    [dateFormat setDateFormat:@"hh:mm a"];
    NSString *timeOnly = [dateFormat stringFromDate:date];

    reservationDateFrom = dateString;
    reservationDateTo = dateOnly;
    reservationTimeTo = timeOnly;
    
    [self.userInfoTableView reloadData];
}

- (void)setupNavigationItem
{
    [self setupRightNavigationItem:self.navigationItem];
    
    UIButton *backBtn = [UIButton buttonWithType: UIButtonTypeCustom];
    [backBtn setImage:[UIImage imageNamed:@"btn_back"] forState:UIControlStateNormal ];
    [backBtn addTarget:self action:@selector(dismissViewController) forControlEvents:UIControlEventTouchUpInside];
    [backBtn setFrame:CGRectMake(10, 0, 15, 20)];
    
    UIBarButtonItem *backBtnItem = [[UIBarButtonItem alloc]initWithCustomView:backBtn];
    
    UIBarButtonItem *leftMostItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    leftMostItem.width = -10.0f;
    
    UIBarButtonItem *fixedItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedItem.width = 10.0f;
    
    self.navigationItem.leftBarButtonItems = @[leftMostItem, fixedItem, backBtnItem];
}

- (void)setupRightNavigationItem:(UINavigationItem *)navigationItem
{
    UIButton* notifBtn = [UIButton buttonWithType: UIButtonTypeCustom];
    [notifBtn setImage:[UIImage imageNamed:@"icon_parkninja"] forState:UIControlStateNormal];
    [notifBtn setImage:[UIImage imageNamed:@"icon_parkninja"] forState:UIControlStateHighlighted];
    [notifBtn setFrame:CGRectMake(10, 0, 35, 35)];
    
    UIBarButtonItem* notif = [[UIBarButtonItem alloc]initWithCustomView: notifBtn];
    
    UIBarButtonItem *rightMostItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    rightMostItem.width = -10.0f;
    
    UIBarButtonItem *fixedItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedItem.width = 10.0f;
    
    NSArray *actionButtonItems = @[rightMostItem, fixedItem, notif];
    navigationItem.rightBarButtonItems = actionButtonItems;
}

- (void)setupNavigationBar
{
    UILabel *titleLabelView = [[UILabel alloc] initWithFrame:CGRectMake(50, 0, 200, 40)]; //<<---- Actually will be auto-resized according to frame of navigation bar;
    [titleLabelView setBackgroundColor:[UIColor clearColor]];
    [titleLabelView setTextAlignment: NSTextAlignmentCenter];
    [titleLabelView setTextColor:[UIColor whiteColor]];
    [titleLabelView setFont:[UIFont fontWithName:@"FSElliotPro-Bold" size: 20]]; //<<--- Greatest font size
    [titleLabelView setAdjustsFontSizeToFitWidth:YES]; //<<---- Allow shrink
    titleLabelView.text = @"User Information";
    self.navigationItem.titleView = titleLabelView;
}

- (void)dismissViewController
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)showList
{
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Car List" message:nil preferredStyle:UIAlertControllerStyleActionSheet];

    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
    }]];

    for (int i = 0; i < [self.carListArray count]; i++)
    {
        CarModel *carModel = [self.carListArray objectAtIndex:i];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:carModel.make style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self refreshTableViewWithCarModel:carModel];
        }]];
    }
    
    [self presentViewController:actionSheet animated:YES completion:nil];
}

- (void)refreshTableViewWithCarModel:(CarModel *)model
{
    [self.view endEditing:YES];
    
    car = model;
    
    [self.userInfoTableView reloadData];
}

- (BOOL)checkAllUserInput
{
    BOOL isValid = YES;
    
    if (![GlobalMethods checkInputForWhiteSpaces:userModel.mobileNo] || ![GlobalMethods checkInputForWhiteSpaces:car.make] ||
         ![GlobalMethods checkInputForWhiteSpaces:car.model] || ![GlobalMethods checkInputForWhiteSpaces:car.color]||
         ![GlobalMethods checkInputForWhiteSpaces:car.plateNum])
        isValid = NO;
    
    return isValid;
}

- (BOOL)checkPromoCodeInput
{
    BOOL isValid = YES;
    
    if (![GlobalMethods checkInputForWhiteSpaces:self.estimateModel.promoCode])
        isValid = NO;
    
    return isValid;
}

- (BOOL)checkTimeInput
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setTimeZone:[NSTimeZone timeZoneWithName:@"PHT"]];
    [dateFormat setDateFormat:@"MM/dd/yyyy hh:mm a"];
    NSString *timeString = [dateFormat stringFromDate:self.datePickerView.date];
    NSDate *selectedDate = [dateFormat dateFromString:timeString];
    
    NSDate *dateOne= [dateFormat dateFromString:reservationDateFrom];
    NSDate *dateTwo = [dateFormat dateFromString:timeString];
    
    NSDateFormatter *dateFormat1 = [[NSDateFormatter alloc] init];
    [dateFormat1 setTimeZone:[NSTimeZone timeZoneWithName:@"PHT"]];
    [dateFormat1 setDateFormat:@"hh:mm a"];
    NSString *reserveTimeTo = [dateFormat1 stringFromDate:self.datePickerView.date];
    
    
   // NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
   // NSDateComponents *components = [[NSDateComponents alloc] init];
   // [components setHour:8];
    
  //  NSDate *dateInput = [calendar dateByAddingComponents:components toDate:selectedDate options:0];
    long timestamp = ([selectedDate timeIntervalSince1970] * 1000);
    
    NSLog(@"DATE: %@", [NSDate dateWithTimeIntervalSince1970:(timestamp / 1000)]);
    
    switch ([dateOne compare:dateTwo])
    {
        case NSOrderedAscending:
        {
            self.estimateModel.reserveTo = timestamp;
            reservationTimeTo = reserveTimeTo;
            return YES;
        }
        case NSOrderedSame:
        case NSOrderedDescending:
        {
            return NO;
        }
     
    }
}

- (void)searchInputtedPromoCode
{
    for (PromoCodeModel *model in self.promoListArray)
    {
        if ([model.code isEqualToString:self.estimateModel.promoCode])
        {
            self.promoCodeModel = model;
            
            if ([GlobalMethods isConnected])
                [self requestReserveEstimation];
            
            return;
        }
    }
    
    UIAlertController *alert = [GlobalMethods showAlertViewWithTitle:nil message:@"Promo code is invalid."];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - IBAction
- (IBAction)cancel:(id)sender
{
    [self dismissViewController];
}

- (IBAction)showTimePickerView:(id)sender
{
    [self.view endEditing:YES];
    self.buttonTag = [sender tag];
    self.pickerContainerView.hidden = NO;
}

- (IBAction)hideDatePickerView:(id)sender
{
    self.pickerContainerView.hidden = YES;
}
- (IBAction)showExistingCars:(id)sender
{
    [self showList];
}
- (IBAction)showSummary:(id)sender
{
    [self.view endEditing:YES];
    
    if ([self checkAllUserInput] && [self checkTimeInput])
    {
        if ([self checkPromoCodeInput])
            [self searchInputtedPromoCode];
        
        else
        {
            self.promoCodeModel = nil;
            
            if ([GlobalMethods isConnected])
                [self requestReserveEstimation];
        }
        
        return;
    }
    
    UIAlertController *alert = [GlobalMethods showAlertViewWithTitle:nil message:MESSAGE_FIELDS_INCOMPLETE];
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)donePickerView:(id)sender
{
    if ([self checkTimeInput])
    {
        [self.userInfoTableView reloadData];
        self.pickerContainerView.hidden = YES;
        return;
    }
    
    UIAlertController *alert = [GlobalMethods showAlertViewWithTitle:nil message:MESSAGE_TIME_INVALID];
    [self presentViewController:alert animated:YES completion:nil];
}


#pragma mark - API Request
- (void)requestCarDetails
{
    MBProgressHUD *HUD = [MBProgressHUD showHUDAddedTo:[[UIApplication sharedApplication] keyWindow] animated:YES];
    HUD.dimBackground = YES;
    HUD.labelText = @"Loading";
    
    self.carListArray = nil;
    
    self.carListArray = [NSMutableArray array];
    
    NSDictionary *userInfo = [[NSDictionary alloc] initWithObjectsAndKeys:userModel.email, @"email", nil];
    
    NSLog(@"PARAMETERS: %@", userInfo);
    
    [[HttpManagerBlocks sharedInstance] postToUrl:URL_CAR_LIST withParameters:userInfo withCompletionBlockSuccess:^(NSDictionary *responseDictionary, RESPONSE_CODE responseCode) {
        
        for (NSArray *array in [responseDictionary objectForKey:@"results"])
        {
            CarModel *carModel = [[CarModel alloc] initWithCarDictionary:(NSDictionary *)array];
            [self.carListArray addObject:carModel];
        }
        
        [MBProgressHUD hideAllHUDsForView:[[UIApplication sharedApplication] keyWindow] animated:YES];
        
    } failure:^(NSError *error, NSString *customDescription,NSString *serverError) {
        [MBProgressHUD hideAllHUDsForView:[[UIApplication sharedApplication] keyWindow] animated:YES];
    }];
}

- (void)requestPromoList
{
    MBProgressHUD *HUD = [MBProgressHUD showHUDAddedTo:[[UIApplication sharedApplication] keyWindow] animated:YES];
    HUD.dimBackground = YES;
    HUD.labelText = @"Loading";
    
    self.promoListArray = nil;
    
    self.promoListArray = [NSMutableArray array];
    
    NSDictionary *userInfo = [[NSDictionary alloc] initWithObjectsAndKeys:userModel.email, @"email", nil];
    
    NSLog(@"PARAMETERS: %@", userInfo);
    
    [[HttpManagerBlocks sharedInstance] postToUrl:URL_PROMO_CODE_LIST withParameters:userInfo withCompletionBlockSuccess:^(NSDictionary *responseDictionary, RESPONSE_CODE responseCode) {
        
        for (NSArray *array in [responseDictionary objectForKey:@"results"])
        {
            PromoCodeModel *codeModel = [[PromoCodeModel alloc] initPromoCodeWithDictionary:(NSDictionary *)array];
            [self.promoListArray addObject:codeModel];
        }

        [MBProgressHUD hideAllHUDsForView:[[UIApplication sharedApplication] keyWindow] animated:YES];
        
    } failure:^(NSError *error, NSString *customDescription,NSString *serverError) {
        [MBProgressHUD hideAllHUDsForView:[[UIApplication sharedApplication] keyWindow] animated:YES];
    }];
}

- (void)requestReserveEstimation
{
    MBProgressHUD *HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.dimBackground = YES;
    HUD.labelText = @"Loading";
    
    self.estimateModel.earlyBird = FALSE;
    
    NSMutableDictionary *userInfo = [[NSMutableDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithLong:serverTime], @"reserveFrom", [NSNumber numberWithLong:self.estimateModel.reserveTo], @"reserveTo", userModel.email, @"email", [NSNumber numberWithBool:self.estimateModel.earlyBird], @"earlyBird", [NSNumber numberWithInteger:self.parkingLotModel.parkingId], @"parkingLotId", nil];
    
    if (self.estimateModel.promoCode.length !=0)
        [userInfo setObject:self.estimateModel.promoCode forKey:@"promoCode"];
    
    NSLog(@"USER INFO: %@", userInfo);
    
    [[HttpManagerBlocks sharedInstance] postToUrl:URL_RESERVE_ESTIMATION withParameters:userInfo withCompletionBlockSuccess:^(NSDictionary *responseDictionary, RESPONSE_CODE responseCode) {
        
        self.estimateModel = [[ParkingReserveEstimationModel alloc] initParkingEstimationWithDictionary:responseDictionary];
        
        self.estimateModel.mobileNum = [userModel.mobileNo integerValue];
        
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
        [self performSegueWithIdentifier:SEGUE_SHOW_PARKING_SUMMARY sender:self];
        
    } failure:^(NSError *error, NSString *customDescription, NSString *serverError) {
        
        UIAlertController *alert =  [GlobalMethods showAlertViewWithTitle:nil message:serverError];
        [self presentViewController:alert animated:YES completion:nil];
        
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
    }];
}

- (void)requestCurrentServerTime
{
    [[HttpManagerBlocks sharedInstance] getTimeFromUrl:URL_CURRENT_SERVER_TIME withParameters:nil withCompletionBlockSuccess:^(NSString *responseTime) {
        
        [self setCurrentDateTimeWithMS:[responseTime longLongValue]];
        
    } failure:^(NSError *error, NSString *customDescription, NSString *serverError) {
        
        NSLog(@"SERVER ERROR: %@", serverError);
        
    }];
}


#pragma mark - ParkingInfoCell
- (UserInformationCell *)setUserInfoWithRow:(NSInteger)row
{
    UserInformationCell *cell = [self.userInfoTableView dequeueReusableCellWithIdentifier:@"UserInformationCell"];
    
    if (cell == nil)
    {
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"UserInformationCell" owner:self options:nil];
        cell = (UserInformationCell *)nibArray[0];
    }
    
    cell.mobileNumberTextField.text = userModel.mobileNo;
    
    [cell.reserveFromDateButton setTitle:reservationDateFrom forState:UIControlStateNormal];
    [cell.reserveToDateButton setTitle:reservationDateTo forState:UIControlStateNormal];
    [cell.reserveTimeButton setTitle:reservationTimeTo forState:UIControlStateNormal];
    
    cell.carMakeTextField.text = car.make;
    cell.carModelTextField.text = car.model;
    cell.carColorTextField.text = car.color;
    cell.plateNumTextField.text = car.plateNum;
    
    return cell;
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self setUserInfoWithRow:indexPath.row];
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 370.0f;
}

#pragma mark - UITextFieldDelegate
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = _userInfoTableView.frame;
    if (movedUp)
    {
        rect.size.height -= kKEYBOARD_HEIGHT;
    }
    else
    {
        rect.size.height += kKEYBOARD_HEIGHT;
    }
    
    _userInfoTableView.frame = rect;
    
    [UIView commitAnimations];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self setViewMovedUp:YES];
    
    NSInteger position = UITableViewScrollPositionNone;
    
    if (textField.tag == TF_INDEX_PROMO_CODE)
        position = UITableViewScrollPositionMiddle;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self setViewMovedUp:NO];
    
    NSString *userInput = textField.text;
    
    switch (textField.tag)
    {
        case TF_INDEX_MOBILE:
        {
            userModel.mobileNo = userInput;
            break;
        }
            
        case TF_INDEX_CAR_MAKE:
        {
            car.make = userInput;
            break;
        }
            
        case TF_INDEX_CAR_MODEL:
        {
            car.model = userInput;
            break;
        }
            
        case TF_INDEX_COLOR:
        {
            car.color = userInput;
            break;
        }
            
        case TF_INDEX_PLATE_NUM:
        {
            car.plateNum = userInput;
            break;
        }
            
        case TF_INDEX_PROMO_CODE:
        {
            self.estimateModel.promoCode = userInput;
            break;
        }
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [textField resignFirstResponder];
    }
    return NO; // We do not want UITextField to insert line-breaks.
}

#pragma mark - UIViewDelegate
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    ParkingSummaryVC *vc = (ParkingSummaryVC *)segue.destinationViewController;
    vc.parkingLotModel = self.parkingLotModel;
    vc.estimateModel = self.estimateModel;
    vc.carModel = car;
    vc.promoModel = self.promoCodeModel;
    vc.locationAddress = self.locationAddress;
}

@end
