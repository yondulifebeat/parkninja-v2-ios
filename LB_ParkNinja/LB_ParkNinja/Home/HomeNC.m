//
//  HomeNC.m
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 11/24/15.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import "HomeNC.h"
#import "AppDelegate.h"

#import "HomeWithButtonsVC.h"
#import "HomeWithHoverVC.h"
#import "SWRevealViewController.h"

@interface HomeNC ()

@end

@implementation HomeNC

+ (HomeNC *)sharedInstance
{
    static dispatch_once_t once;
    static HomeNC *manager;
    
    dispatch_once(&once, ^{
        manager = [[HomeNC alloc] init];
    });
    
    return manager;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setNavigationBarHidden:YES];
    
    UIStoryboard *loginSB = [UIStoryboard storyboardWithName:@"HomeSB" bundle:nil];
    HomeWithButtonsVC *vc = [loginSB instantiateViewControllerWithIdentifier:@"HomeWithButtonsVC"];
    
    AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [app setRootViewController:vc];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setHomeHoverVC
{
    NSArray* tempVCA = [self.navigationController viewControllers];
    
    for(UIViewController *tempVC in tempVCA)
    {
      //  if([tempVC isKindOfClass:[urViewControllerClass class]])
       // {
            [tempVC removeFromParentViewController];
       // }
    }
    
    UIStoryboard *loginSB = [UIStoryboard storyboardWithName:@"HomeMapSB" bundle:nil];
    HomeWithHoverVC *vc = [loginSB instantiateViewControllerWithIdentifier:@"HomeWithHoverVC"];
    
    [self presentViewController:vc animated:YES completion:nil];
    
    //AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    //[app setRootViewController:vc];
    
  //  UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
   // SecondViewController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"SecondViewController"];
    
  //  UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:rootViewController];
  //  [navController setViewControllers: @[rootViewController] animated: YES];
    
   //  SWRevealViewController *revealViewController = self.revealViewController;
    
   // [self.revealViewController setFrontViewController:self];
  //  [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
}

@end
