//
//  ParkingSummaryVC.h
//  LB_ParkNinja
//
//  Created by Tric Rullan on 03/12/2015.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParkingLotModel.h"
#import "ParkingReserveEstimationModel.h"
#import "CarModel.h"
#import "PromoCodeModel.h"

@interface ParkingSummaryVC : UIViewController

@property (strong, nonatomic) ParkingLotModel *parkingLotModel;
@property (strong, nonatomic) ParkingReserveEstimationModel *estimateModel;
@property (strong, nonatomic) CarModel *carModel;
@property (strong, nonatomic) PromoCodeModel *promoModel;
@property (strong, nonatomic) NSString *locationAddress;

@end
