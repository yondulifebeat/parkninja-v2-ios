//
//  ParkingSummaryVC.m
//  LB_ParkNinja
//
//  Created by Tric Rullan on 03/12/2015.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import "ParkingSummaryVC.h"
#import "ParkingSummaryCell.h"

#import "GlobalMethods.h"
#import "HttpManagerBlocks.h"
#import "GlobalConstants.h"
#import "UserModel.h"
#import "CarModel.h"
#import "PaymentDetailsVC.h"

@interface ParkingSummaryVC ()

@property (strong, nonatomic) IBOutlet UITableView *parkingSummaryTableView;

@end

@implementation ParkingSummaryVC
{
    UserModel *userModel;
    User *user;
    CarModel *car;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupNavigationBar];
    [self setupNavigationItem];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Private Methods
- (void)setupNavigationItem
{
    [self setupRightNavigationItem:self.navigationItem];
    
    UIButton *backBtn = [UIButton buttonWithType: UIButtonTypeCustom];
    [backBtn setImage:[UIImage imageNamed:@"btn_back"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(dismissViewController) forControlEvents:UIControlEventTouchUpInside];
    [backBtn setFrame:CGRectMake(10, 0, 15, 20)];
    
    UIBarButtonItem *backBtnItem = [[UIBarButtonItem alloc]initWithCustomView:backBtn];
    
    UIBarButtonItem *leftMostItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    leftMostItem.width = -10.0f;
    
    UIBarButtonItem *fixedItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedItem.width = 10.0f;
    
    self.navigationItem.leftBarButtonItems = @[leftMostItem, fixedItem, backBtnItem];
}

- (void)setupRightNavigationItem:(UINavigationItem *)navigationItem
{
    UIButton* notifBtn = [UIButton buttonWithType: UIButtonTypeCustom];
    [notifBtn setImage:[UIImage imageNamed:@"icon_parkninja"] forState:UIControlStateNormal];
    [notifBtn setImage:[UIImage imageNamed:@"icon_parkninja"] forState:UIControlStateHighlighted];
    [notifBtn setFrame:CGRectMake(10, 0, 35, 35)];
    
    UIBarButtonItem* notif = [[UIBarButtonItem alloc]initWithCustomView: notifBtn];
    
    UIBarButtonItem *rightMostItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    rightMostItem.width = -10.0f;
    
    UIBarButtonItem *fixedItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedItem.width = 10.0f;
    
    NSArray *actionButtonItems = @[rightMostItem, fixedItem, notif];
    navigationItem.rightBarButtonItems = actionButtonItems;
}

- (void)setupNavigationBar
{
    UILabel *titleLabelView = [[UILabel alloc] initWithFrame:CGRectMake(50, 0, 200, 40)]; //<<---- Actually will be auto-resized according to frame of navigation bar;
    [titleLabelView setBackgroundColor:[UIColor clearColor]];
    [titleLabelView setTextAlignment: NSTextAlignmentCenter];
    [titleLabelView setTextColor:[UIColor whiteColor]];
    [titleLabelView setFont:[UIFont fontWithName:@"FSElliotPro-Bold" size: 20]]; //<<--- Greatest font size
    [titleLabelView setAdjustsFontSizeToFitWidth:YES]; //<<---- Allow shrink
    titleLabelView.text = self.parkingLotModel.location;
    self.navigationItem.titleView = titleLabelView;
}

- (void)dismissViewController
{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - IBAction
- (IBAction)showPaymentDetails:(id)sender
{
    [self performSegueWithIdentifier:SEGUE_SHOW_PAYMENT_DETAILS sender:self];
}


#pragma mark - ParkingInfoCell
- (ParkingSummaryCell *)setParkingSummaryWithRow:(NSInteger)row
{
    ParkingSummaryCell *cell = [self.parkingSummaryTableView dequeueReusableCellWithIdentifier:@"ParkingSummaryCell"];
    
    if (cell == nil)
    {
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"ParkingSummaryCell" owner:self options:nil];
        cell = (ParkingSummaryCell *)nibArray[0];
    }
    
    cell.parkingNameLabel.text = self.parkingLotModel.location;
    cell.addressLabel.text = self.locationAddress;
    
    cell.mobileNumLabel.text = [NSString stringWithFormat:@"%ld", (long)self.estimateModel.mobileNum];
    cell.carMakeLabel.text = self.carModel.make;
    cell.carModelLabel.text = self.carModel.model;
    cell.carColorLabel.text = self.carModel.color;
    cell.plateNumLabel.text = self.carModel.plateNum;
    

    NSDate *dateReserveFrom = [NSDate dateWithTimeIntervalSince1970:(self.estimateModel.reserveFrom / 1000)];
        
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setTimeZone:[NSTimeZone timeZoneWithName:@"PHT"]];
    [dateFormat setDateFormat:@"MM/dd/yyyy hh:mm a"];
    
    NSString *dateReserveFromString = [dateFormat stringFromDate:dateReserveFrom];

    NSDate *dateReserveTo = [NSDate dateWithTimeIntervalSince1970:(self.estimateModel.reserveTo / 1000)];
    NSString *dateReserveToString = [dateFormat stringFromDate:dateReserveTo];


    cell.dateReservationLabel.text = [NSString stringWithFormat:@"%@ - %@", dateReserveFromString, dateReserveToString];

    cell.estimatePriceLabel.text = [NSString stringWithFormat:@"Php %ld.00 (Payment computation begins upon finalizing the reservation)", (long)self.estimateModel.estimateAmount];
    
    return cell;
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self setParkingSummaryWithRow:indexPath.row];
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 370.0f;
}

#pragma mark - UIViewDelegate
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    PaymentDetailsVC *vc = (PaymentDetailsVC *)segue.destinationViewController;
    vc.parkingLotModel = self.parkingLotModel;
    vc.estimateModel = self.estimateModel;
    vc.carModel = self.carModel;
    vc.promoModel = self.promoModel;
}

@end
