//
//  HomeSB.m
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 11/16/15.
//  Copyright © 2015 Tricia Rullan. All rights reserved.
//

#import "HomeVC.h"
#import "SWRevealViewController.h"
#import <MBProgressHUD/MBProgressHUD.h>

#import "ParkListingVC.h"
#import "LocationManager.h"
#import "ParkingLotModel.h"

#import "HttpManagerBlocks.h"
#import "GlobalConstants.h"
#import "GlobalMethods.h"

#import "MapParkingMarker.h"
#import "ParkingLotDetailsVC.h"

#import "SearchResultsTVC.h"
#import <Reachability/Reachability.h>

@interface HomeVC ()

@property (weak, nonatomic) IBOutlet GMSMapView *mapView;
@property (weak, nonatomic) IBOutlet UIButton *sideBarBtn;
@property (weak, nonatomic) IBOutlet UIView *buttonViewContainer;

@property (strong, nonatomic) LocationManager *locationManager;
@property (strong, nonatomic) MapParkingMarker *parkingMarker;
@property (strong, nonatomic) ParkingLotDetailsVC *parkingLotDetails;

@property (strong, nonatomic) NSMutableArray *parkingListArray;
@property (strong, nonatomic) NSMutableArray *searchResultsArray;

@property (strong, nonatomic) CLLocation *currentLocation;

@property (strong, nonatomic) NSString *distanceText;
@property (strong, nonatomic) NSString *locationAddress;

@property (nonatomic, assign) BOOL mapDidLoadOnce;
@property (assign, nonatomic) BOOL navigateUser;
@property (assign, nonatomic) BOOL zoomToUserLocation;

@property (weak, nonatomic) IBOutlet UIView *navigationView;

@property (weak, nonatomic) IBOutlet UIView *searchContainerView;


@end

@implementation HomeVC
{
    GMSMarker *userMapMarker;
    GMSPolyline *polyline;
}

+ (HomeVC *)sharedInstance
{
    static dispatch_once_t once;
    static HomeVC *manager;
    
    dispatch_once(&once, ^{
        manager = [[HomeVC alloc] init];
    });
    
    return manager;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navController = self.navigationController;
    
    SWRevealViewController *revealViewController = self.revealViewController;
    
    if (revealViewController)
    {
        [self.sideBarBtn addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    [self setUpView];
    
     self.zoomToUserLocation = YES;
    
    [self initLocationManager];
    
    [self requestAllParkingList:^(BOOL finished) {
        
    }];
    
    self.navigationController.navigationBar.hidden = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(requestNavigationWithNotification:)
                                                 name:@"RequestRoute"
                                               object:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navController.navigationBar.hidden = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
    [self.view endEditing:YES];
}


#pragma mark - Private Methods

- (void)initLocationManager
{
    self.locationManager = [LocationManager sharedInstance];
    [self.locationManager initLocationManager];
    [self.locationManager setDelegate:self];
    
    self.mapDidLoadOnce = NO;
}

- (void)setUpView
{
    [self.mapView setDelegate:self];
    
    self.buttonViewContainer.hidden = [GlobalMethods getUserStatus];
    //self.navigationView.hidden = ![GlobalMethods getUserStatus];
}

- (void)displayContentController:(UIViewController *)content
{
    [GlobalMethods saveUserStatus:YES];
    self.buttonViewContainer.hidden = [GlobalMethods getUserStatus];
    
    [self addChildViewController:content];
    [self.contentView addSubview:content.view];
    [content didMoveToParentViewController:self];
}

- (void)hideContentController:(UIViewController *)content
{
    self.buttonViewContainer.hidden = [GlobalMethods getUserStatus];
    
    [content willMoveToParentViewController:nil];
    [content.view removeFromSuperview];
    [content removeFromParentViewController];
}

- (void)setMapParkingPins:(ParkingLotModel *)parkingLot
{
    MapParkingMarker *marker = [[MapParkingMarker alloc] init];
    marker.parkingLot = parkingLot;
    marker.title = parkingLot.location;
    marker.position = CLLocationCoordinate2DMake(parkingLot.latitude, parkingLot.longitude);
    marker.icon = [self setParkingPinsColorWithModel:parkingLot];
    marker.map = self.mapView;
}
- (UIImage *)setParkingPinsColorWithModel:(ParkingLotModel *)model
{
    NSInteger takenSlots = 0;
    
    if (model.occupiedSlots <= 0)
        takenSlots = 0;
    else
        takenSlots = (model.occupiedSlots / model.totalSlots) * 100;
    
    NSString *pinName = PIN_RED;
    
    if (takenSlots < 40)
        pinName = PIN_GREEN;
    else if (takenSlots >= 40 && takenSlots < 80)
        pinName = PIN_YELLOW;
    else if (takenSlots >= 80 && takenSlots <= 99)
        pinName = PIN_ORANGE;
    else
        pinName = PIN_RED;
    
    return [self image:[UIImage imageNamed:pinName]];
}

- (void)removePolyLine
{
    [polyline setMap:nil];
}

- (void)removeMarkers
{
    [userMapMarker setMap:nil];
    [self.parkingMarker setMap:nil];
}

- (UIImage *)image:(UIImage *)image
{
    CGRect rect = CGRectMake(0, 0, image.size.width / 1.5, image.size.height / 1.5);
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, 0);
    [image drawInRect:rect];
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

#pragma  mark - LocationManagerDelegate
- (void)getCurrentUserLocation:(CLLocation *)location
{
    if (!self.mapDidLoadOnce)
    {
        [self.mapView animateToLocation:location.coordinate];
        self.mapDidLoadOnce = TRUE;
        self.currentLocation = location;
        
        if (self.zoomToUserLocation)
            [self.mapView animateToZoom:13];
        
        userMapMarker = [[GMSMarker alloc] init];
        userMapMarker.position = self.currentLocation.coordinate;
        userMapMarker.snippet = @"Current Location";
        userMapMarker.icon = [self image:[UIImage imageNamed:@"pin_current_loc"]];
        userMapMarker.map = self.mapView;
        
        [self.locationManager.locationManager stopUpdatingLocation];
    }
}

#pragma mark - UIViewDelegate
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"showSearchResults"])
    {
        SearchResultsTVC *vc = (SearchResultsTVC *)segue.destinationViewController;
        vc.parkingListArray = self.parkingListArray;
        return;
    }
    
    ParkingLotDetailsVC *vc = (ParkingLotDetailsVC *)segue.destinationViewController;
    vc.model = self.parkingMarker.parkingLot;
    
    vc.distance = self.distanceText;
    vc.locationAddress = self.locationAddress;
}

#pragma mark - GMSMapViewDelegate
- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker
{
    mapView.selectedMarker = marker;

    if (marker != userMapMarker && [GlobalMethods isConnected])
       [self requestNavigationWithMarker:marker];
    
    return YES;
}

- (void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker
{
    if (marker != userMapMarker && [GlobalMethods isConnected])
    {
        self.parkingMarker = (MapParkingMarker *)marker;
        [self performSegueWithIdentifier:SEGUE_SHOW_PARKING_LOT_DETAILS sender:self];
    }
}

#pragma mark - API Request
- (void)requestNavigationWithMarker:(GMSMarker *)marker
{
    self.parkingMarker = (MapParkingMarker *)marker;
    [self removePolyLine];
    
    MBProgressHUD *HUD = [MBProgressHUD showHUDAddedTo:[[UIApplication sharedApplication] keyWindow] animated:YES];
    HUD.dimBackground = YES;
    HUD.labelText = @"Loading";
    
    NSString *parkingCoordinate = [NSString stringWithFormat:@"%f,%f", self.parkingMarker.parkingLot.latitude, self.parkingMarker.parkingLot.longitude];
    
    NSString *baseUrl = [NSString stringWithFormat:URL_REQUEST_GOOGLE_ROUTE, self.currentLocation.coordinate.latitude, self.currentLocation.coordinate.longitude, parkingCoordinate];
    
    NSLog(@"GOOGLE: %@", baseUrl);
    
    [[HttpManagerBlocks sharedInstance] getFromUrl:baseUrl withParameters:nil withCompletionBlockSuccess:^(NSDictionary *responseDictionary, RESPONSE_CODE responseCode) {
        
        NSDictionary *routes = responseDictionary[@"routes"][0];
        NSDictionary *route = routes[@"overview_polyline"];
        NSString *overview_route = route[@"points"];
        
        
        NSDictionary *legs = routes[@"legs"][0];
        NSDictionary *distance = legs[@"distance"];
        self.distanceText = distance[@"text"];
        self.locationAddress = legs[@"end_address"];
        
        
        NSLog(@"DISTANCE: %@", self.distanceText);
        
        self.parkingMarker.snippet = [NSString stringWithFormat:@"%@ %@", self.parkingMarker.parkingLot.location, self.distanceText];
        self.parkingMarker.parkingLot.distance = self.distanceText;
        
        GMSPath *path = [GMSPath pathFromEncodedPath:overview_route];
        polyline = [GMSPolyline polylineWithPath:path];
        polyline.strokeWidth = 4;
        polyline.strokeColor = [UIColor colorWithRed:0.05 green:0.61 blue:0.67 alpha:1.0];
        polyline.map = self.mapView;
        
        CLLocationCoordinate2D centerCoordinate = CLLocationCoordinate2DMake((self.currentLocation.coordinate.latitude + self.parkingMarker.parkingLot.latitude)/2, (self.currentLocation.coordinate.longitude + self.parkingMarker.parkingLot.longitude)/2);
        
        [self.mapView animateToLocation:centerCoordinate];
        [self.mapView animateToZoom:13];
        
        [MBProgressHUD hideAllHUDsForView:[[UIApplication sharedApplication] keyWindow] animated:YES];

        
    } failure:^(NSError *error, NSString *customDescription, NSString *serverError) {
        
        [MBProgressHUD hideAllHUDsForView:[[UIApplication sharedApplication] keyWindow] animated:YES];
        
    }];
}

- (void)requestAllParkingList:(void(^)(BOOL finished))completion
{
    self.parkingListArray = [NSMutableArray array];
    
    NSMutableArray *saveArray = [NSMutableArray array];
    
    [[HttpManagerBlocks sharedInstance] getFromUrl:URL_PARK_LIST withParameters:nil withCompletionBlockSuccess:^(NSDictionary *responseDictionary, RESPONSE_CODE responseCode){
        
        for (NSArray *array in [responseDictionary objectForKey:@"results"])
        {
            ParkingLotModel *parkingModel = [[ParkingLotModel alloc] initWithParkingLotDictionary:(NSDictionary *)array];
            [self.parkingListArray addObject:parkingModel];
            [self setMapParkingPins:parkingModel];
            
            [saveArray addObject:array];
        }
        
        [GlobalMethods saveParkingList:saveArray];

        completion(YES);
        
    } failure:^(NSError *error, NSString *customDescription, NSString *serverError) {
        
        NSLog(@"SERVER ERROR: %@", serverError);
        
//        [self requestAllParkingList:^ (BOOL finished) {
//
//        }];
        
        completion(YES);
    }];
}

- (BOOL)isSystemVersionEight
{
    return ([[[UIDevice currentDevice] systemVersion] integerValue] >= 8.0);
}

- (BOOL)isLocationServicesAuthorized
{
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusRestricted)
    {
        if ([self isSystemVersionEight])
        {
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Location Services" message:@"This application needs location services enabled. Please go to settings and allow location access for this app." preferredStyle:UIAlertControllerStyleAlert];
            
            
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDestructive
                                                           handler:^(UIAlertAction *action) {
                                                               [alert dismissViewControllerAnimated:YES completion:nil];
                                                           }];
            
            UIAlertAction *settings = [UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction *action) {
                        
                                                               [[UIApplication sharedApplication] openURL: [NSURL URLWithString: UIApplicationOpenSettingsURLString]];
                                                           }];
            
            [alert addAction:settings];
            [alert addAction:cancel];
            
            [self presentViewController:alert animated:YES completion:nil];

            return NO;
        }
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Location Services" message:@"This application needs location services enabled. Please go to settings and allow location access for this app." preferredStyle:UIAlertControllerStyleAlert];
        
        
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction *action) {
                                                           [alert dismissViewControllerAnimated:YES completion:nil];
                                                       }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
        
        return NO;
    }
    
    return YES;
}

- (void)requestNavigationWithNotification:(NSNotification *)notification
{
    if (![self isLocationServicesAuthorized])
        return;
    
    NSDictionary *dict = notification.userInfo;
    ParkingLotModel *model = [dict objectForKey:@"ParkingModel"];
    
    MBProgressHUD *HUD = [MBProgressHUD showHUDAddedTo:[[UIApplication sharedApplication] keyWindow] animated:YES];
    HUD.dimBackground = YES;
    HUD.labelText = @"Loading";
    
    NSString *parkingCoordinate = [NSString stringWithFormat:@"%f,%f", model.latitude, model.longitude];
    
    NSString *baseUrl = [NSString stringWithFormat:URL_REQUEST_GOOGLE_ROUTE, self.currentLocation.coordinate.latitude, self.currentLocation.coordinate.longitude, parkingCoordinate];
    
    NSLog(@"GOOGLE: %@", baseUrl);
    
    [[HttpManagerBlocks sharedInstance] getFromUrl:baseUrl withParameters:nil withCompletionBlockSuccess:^(NSDictionary *responseDictionary, RESPONSE_CODE responseCode) {
        
        NSDictionary *routes = responseDictionary[@"routes"][0];
        NSDictionary *route = routes[@"overview_polyline"];
        NSString *overview_route = route[@"points"];
        
        
        NSDictionary *legs = routes[@"legs"][0];
        NSDictionary *distance = legs[@"distance"];
        NSString *distanceText = distance[@"text"];
        
        NSLog(@"DISTANCE: %@", distanceText);
        
        self.parkingMarker.snippet = [NSString stringWithFormat:@"%@ %@", self.parkingMarker.parkingLot.location, distanceText];
        self.parkingMarker.parkingLot.distance = distanceText;
        
        [self removePolyLine];
        
        GMSPath *path = [GMSPath pathFromEncodedPath:overview_route];
        polyline = [GMSPolyline polylineWithPath:path];
        polyline.strokeWidth = 3;
        polyline.map = self.mapView;
        
        CLLocationCoordinate2D centerCoordinate = CLLocationCoordinate2DMake((self.currentLocation.coordinate.latitude + model.latitude)/2, (self.currentLocation.coordinate.longitude + model.longitude)/2);
        
        [self.mapView animateToLocation:centerCoordinate];
        [self.mapView animateToZoom:13];
        
        [MBProgressHUD hideAllHUDsForView:[[UIApplication sharedApplication] keyWindow] animated:YES];
        
    } failure:^(NSError *error, NSString *customDescription, NSString *serverError) {
        
        [MBProgressHUD hideAllHUDsForView:[[UIApplication sharedApplication] keyWindow] animated:YES];
        NSLog(@"SERVER ERROR: %@", serverError);
        
    }];
}

#pragma mark - UIButtonAction
- (IBAction)findParking:(id)sender
{
    if ([GlobalMethods isConnected])
    {
        [self removeMarkers];
        
        userMapMarker = [[GMSMarker alloc] init];
        userMapMarker.position = self.currentLocation.coordinate;
        userMapMarker.snippet = @"Current Location";
        userMapMarker.icon = [self image:[UIImage imageNamed:@"pin_current_loc"]];
        userMapMarker.map = self.mapView;


    [self requestAllParkingList: ^(BOOL finished)
    {
        if (finished)
        {
            UIAlertController *alert = [GlobalMethods showAlertViewWithTitle:nil message:[NSString stringWithFormat:@"There are %lu parking spaces available.", (unsigned long)[self.parkingListArray count]]];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }];
    
        [self.mapView animateToLocation:self.currentLocation.coordinate];
        [self.mapView animateToZoom:12];
    
        self.navigationView.hidden = YES;
        self.buttonViewContainer.hidden = YES;
    }
}

- (IBAction)showParkListing:(id)sender
{
    self.navigationView.hidden = YES;
    
    UIStoryboard *loginSB = [UIStoryboard storyboardWithName:@"HomeSB" bundle:nil];
    ParkListingVC *vc = [loginSB instantiateViewControllerWithIdentifier:@"ParkListingVC"];
    
    [self displayContentController:vc];
}

- (IBAction)showSideBar:(id)sender
{
    [self.view endEditing:YES];
}

- (IBAction)showNavigationContainerView:(id)sender
{
    self.navigationView.hidden = NO;
}

- (IBAction)hideNavigationContainerView:(id)sender
{
    self.navigationView.hidden = YES;
}

- (IBAction)findMe:(id)sender
{
    if ([GlobalMethods isConnected])
    {
        [userMapMarker setMap:nil];
        
        self.zoomToUserLocation = NO;
        [self initLocationManager];
        self.navigationView.hidden = YES;
    }
}

#pragma mark - UITextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self.view endEditing:YES];
    
    [self performSegueWithIdentifier:SEGUE_SHOW_SEARCH_RESULTS sender:self];
}

@end
