//
//  PaymentDetailsVC.m
//  LB_ParkNinja
//
//  Created by Tricia Rullan on 1/4/16.
//  Copyright © 2016 Tricia Rullan. All rights reserved.
//

#import "PaymentDetailsVC.h"
#import "PaymentCell.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "GlobalConstants.h"
#import "GlobalMethods.h"
#import "HttpManagerBlocks.h"
#import "PaymentVC.h"

#import "User.h"
#import "UserModel.h"
#import "CardModel.h"

#import "ETicketVC.h"
#import "Flurry.h"

@interface PaymentDetailsVC ()

@property (weak, nonatomic) IBOutlet UITableView *paymentDetailsTableView;
@property (strong, nonatomic) NSMutableArray *cardListArray;

@property (strong, nonatomic) CardModel *cardModel;

@property (assign, nonatomic) BOOL hasSelectedCardFromList;
@property (strong, nonatomic) NSString *qrCode;

@end

@implementation PaymentDetailsVC
{
    User *user;
    UserModel *userModel;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupNavigationBar];
    [self setupNavigationItem];
    
    user = [GlobalMethods getUser];
    userModel = [[UserModel alloc] initWithUserModel:user];
    
    self.hasSelectedCardFromList = NO;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self requestCardList];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Private Methods
- (void)setupNavigationItem
{
    [self setupRightNavigationItem:self.navigationItem];
    
    UIButton *backBtn = [UIButton buttonWithType: UIButtonTypeCustom];
    [backBtn setImage:[UIImage imageNamed:@"btn_back"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(dismissViewController) forControlEvents:UIControlEventTouchUpInside];
    [backBtn setFrame:CGRectMake(10, 0, 15, 20)];
    
    UIBarButtonItem *backBtnItem = [[UIBarButtonItem alloc]initWithCustomView:backBtn];
    
    UIBarButtonItem *leftMostItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    leftMostItem.width = -10.0f;
    
    UIBarButtonItem *fixedItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedItem.width = 10.0f;
    
    self.navigationItem.leftBarButtonItems = @[leftMostItem, fixedItem, backBtnItem];
}

- (void)setupRightNavigationItem:(UINavigationItem *)navigationItem
{
    UIButton* notifBtn = [UIButton buttonWithType: UIButtonTypeCustom];
    [notifBtn setImage:[UIImage imageNamed:@"icon_parkninja"] forState:UIControlStateNormal];
    [notifBtn setImage:[UIImage imageNamed:@"icon_parkninja"] forState:UIControlStateHighlighted];
    [notifBtn setFrame:CGRectMake(10, 0, 35, 35)];
    
    UIBarButtonItem* notif = [[UIBarButtonItem alloc]initWithCustomView: notifBtn];
    
    UIBarButtonItem *rightMostItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    rightMostItem.width = -10.0f;
    
    UIBarButtonItem *fixedItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedItem.width = 10.0f;
    
    NSArray *actionButtonItems = @[rightMostItem, fixedItem, notif];
    navigationItem.rightBarButtonItems = actionButtonItems;
}

- (void)setupNavigationBar
{
    UILabel *titleLabelView = [[UILabel alloc] initWithFrame:CGRectMake(50, 0, 200, 40)]; //<<---- Actually will be auto-resized according to frame of navigation bar;
    [titleLabelView setBackgroundColor:[UIColor clearColor]];
    [titleLabelView setTextAlignment: NSTextAlignmentCenter];
    [titleLabelView setTextColor:[UIColor whiteColor]];
    [titleLabelView setFont:[UIFont fontWithName:@"FSElliotPro-Bold" size: 20]]; //<<--- Greatest font size
    [titleLabelView setAdjustsFontSizeToFitWidth:YES]; //<<---- Allow shrink
    titleLabelView.text = @"Payment Details";
    self.navigationItem.titleView = titleLabelView;
}

- (void)dismissViewController
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)showList
{
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Card List" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
    }]];
    
    for (int i = 0; i < [self.cardListArray count]; i++)
    {
        CardModel *cardModel = [self.cardListArray objectAtIndex:i];
        
        NSString *title = [NSString stringWithFormat:@"%@ - %@", cardModel.type, cardModel.maskedNum];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:title style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            self.hasSelectedCardFromList = YES;
            [self refreshTableViewWithCarModel:cardModel];
        }]];
    }
    
    if ([self.cardListArray count] > 0) {
         [self presentViewController:actionSheet animated:YES completion:nil];
    }
}

- (void)refreshTableViewWithCarModel:(CardModel *)model
{
    self.cardModel = model;
    
    [self.paymentDetailsTableView reloadData];
}

- (BOOL)checkPromoCodeInput
{
    BOOL isValid = YES;
    
    if (![GlobalMethods checkInputForWhiteSpaces:self.promoModel.code] || ![GlobalMethods checkInputForWhiteSpaces:[NSString stringWithFormat:@"%ld", (long)self.promoModel.promoCodeId]])
        isValid = NO;
    
    return isValid;
}

#pragma mark - UIButtonAction
- (IBAction)showExisitingCardList:(id)sender
{
    [self showList];
}

- (IBAction)addNewCard:(id)sender
{
    [self performSegueWithIdentifier:SEGUE_SHOW_PAYMENT sender:self];
}

- (IBAction)continuePurchasing:(id)sender
{
    if (self.cardModel != nil && [GlobalMethods isConnected])
    {
        [self requestProceedPurchase];;
        return;
    }
    
    UIAlertController *alert = [GlobalMethods showAlertViewWithTitle:nil message:@"Select a credit card"];
    [self presentViewController:alert animated:YES completion:nil];
    
}

#pragma mark - API Request
- (void)requestCardList
{
    MBProgressHUD *HUD = [MBProgressHUD showHUDAddedTo:[[UIApplication sharedApplication] keyWindow] animated:YES];
    HUD.dimBackground = YES;
    HUD.labelText = @"Loading";
    
    self.cardListArray = nil;
    
    self.cardListArray = [NSMutableArray array];
    
    NSDictionary *userInfo = [[NSDictionary alloc] initWithObjectsAndKeys: userModel.email, @"email", nil];
    
    NSLog(@"PARAMETERS: %@", userInfo);
    
    [[HttpManagerBlocks sharedInstance] postToUrl:URL_CARD_LIST withParameters:userInfo withCompletionBlockSuccess:^(NSDictionary *responseDictionary, RESPONSE_CODE responseCode) {
        
        for (NSArray *array in [responseDictionary objectForKey:@"results"])
        {
            NSLog(@"RESPONSE ARRAY: %@", array);
            CardModel *cardModel = [[CardModel alloc] initCardModelWithDictionary:(NSDictionary *)array];
            [self.cardListArray addObject:cardModel];
        }
        
        [MBProgressHUD hideAllHUDsForView:[[UIApplication sharedApplication] keyWindow] animated:YES];
        
    } failure:^(NSError *error, NSString *customDescription,NSString *serverError) {
        [MBProgressHUD hideAllHUDsForView:[[UIApplication sharedApplication] keyWindow] animated:YES];
    }];
}

- (void)requestProceedPurchase
{
    MBProgressHUD *HUD = [MBProgressHUD showHUDAddedTo:[[UIApplication sharedApplication] keyWindow] animated:YES];
    HUD.dimBackground = YES;
    HUD.labelText = @"Loading";
    
    NSMutableDictionary *userInfo = [NSMutableDictionary dictionary];
    
    if (self.carModel.carId != 0)
    {
        userInfo = [[NSMutableDictionary alloc] initWithObjectsAndKeys: userModel.email, @"email", [NSNumber numberWithLong:self.estimateModel.reserveFrom], @"reserveFrom", [NSNumber numberWithLong:self.estimateModel.reserveTo], @"reserveTo", [NSNumber numberWithBool:self.estimateModel.earlyBird], @"earlyBird", [NSNumber numberWithInteger:self.parkingLotModel.parkingId], @"parkingLotId", [NSNumber numberWithInteger:self.carModel.carId], @"carId", [NSNumber numberWithInteger:self.estimateModel.estimateAmount], @"estimateAmount", [NSNumber numberWithInteger:self.parkingLotModel.merchantPartnerId], @"merchantPartnerId", [NSNumber numberWithInteger:self.cardModel.cardId], @"cardId", self.parkingLotModel.location, @"parkingLocation", [NSNumber numberWithInteger:self.estimateModel.verifyAmount], @"verifyAmount", nil];
    } else
    {
        userInfo = [[NSMutableDictionary alloc] initWithObjectsAndKeys: userModel.email, @"email", [NSNumber numberWithLong:self.estimateModel.reserveFrom], @"reserveFrom", [NSNumber numberWithLong:self.estimateModel.reserveTo], @"reserveTo", [NSNumber numberWithBool:self.estimateModel.earlyBird], @"earlyBird", [NSNumber numberWithInteger:self.parkingLotModel.parkingId], @"parkingLotId", self.carModel.make, @"make", self.carModel.model, @"model", self.carModel.color, @"color", self.carModel.plateNum, @"plateNo", [NSNumber numberWithInteger:self.estimateModel.estimateAmount], @"estimateAmount", [NSNumber numberWithInteger:self.parkingLotModel.merchantPartnerId], @"merchantPartnerId", [NSNumber numberWithInteger:self.cardModel.cardId], @"cardId", self.parkingLotModel.location, @"parkingLocation", [NSNumber numberWithInteger:self.estimateModel.verifyAmount], @"verifyAmount", nil];
    }

    if ([self checkPromoCodeInput])
        [userInfo setObject:[NSNumber numberWithInteger:self.promoModel.promoCodeId] forKey:@"promoCodeId"];
    
        
    NSLog(@"PARAMETERS: %@", userInfo);
    
    [[HttpManagerBlocks sharedInstance] postPurchaseParkingToUrl:URL_PROCEED_PURCHASE withParameters:userInfo withCompletionBlockSuccess:^(NSString *response) {
        
        self.qrCode = response;
        
        NSDictionary *params =[[NSDictionary alloc] initWithObjectsAndKeys:self.parkingLotModel.location, @"parkingLocation", [NSNumber numberWithLong:self.estimateModel.reserveFrom], @"reserveFrom", [NSNumber numberWithLong:self.estimateModel.reserveTo], @"reserveTo",  nil];
         // [Flurry logEvent:@"Parking_Reserve" withParameters:params];
        
        [self performSegueWithIdentifier:SEGUE_SHOW_ETICKET sender:self];
        
        [MBProgressHUD hideAllHUDsForView:[[UIApplication sharedApplication] keyWindow] animated:YES];
        
    } failure:^(NSError *error, NSString *customDescription,NSString *serverError) {
       
        UIAlertController *alert = [GlobalMethods showAlertViewWithTitle:nil message:serverError];
        [self presentViewController:alert animated:YES completion:nil];
        
        [MBProgressHUD hideAllHUDsForView:[[UIApplication sharedApplication] keyWindow] animated:YES];
    }];
}

#pragma mark - ParkingInfoCell
- (PaymentCell *)setPaymentCellWithRow:(NSInteger)row
{
    PaymentCell *cell = [self.paymentDetailsTableView dequeueReusableCellWithIdentifier:@"PaymentCell"];
    
    if (cell == nil)
    {
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"PaymentCell" owner:self options:nil];
        cell = (PaymentCell *)nibArray[0];
    }
    
    if (self.hasSelectedCardFromList)
        [cell.selectCreditCardBtn setTitle:self.cardModel.maskedNum forState:UIControlStateNormal];
    
    else
        [cell.selectCreditCardBtn setTitle:@"Select a Credit Card" forState:UIControlStateNormal];
    
    return cell;
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self setPaymentCellWithRow:indexPath.row];
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 135.0f;
}

#pragma mark - UIViewDelegate
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:SEGUE_SHOW_PAYMENT])
    {
        PaymentVC *vc = (PaymentVC *)segue.destinationViewController;
        vc.isFromPaymentDetails = TRUE;
        return;
    }
    
    ETicketVC *vc = (ETicketVC *)segue.destinationViewController;
    vc.qrCode = self.qrCode;
    vc.parkingLotModel = self.parkingLotModel;
    vc.estimateModel = self.estimateModel;
    vc.carModel = self.carModel;
}

@end
